import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { CanActivateViaAuthGuard } from './authAndProfile/auth.guard';

import { ShowsComponent } from './shows/shows.component';
import { ShowDetailComponent } from './shows/showDetail.component';

import { VenueListComponent } from './venues/venueList.component';
import { VenueDetailComponent } from './venues/venueDetail.component';

import { MovieListComponent } from './movies/movieList.component';
import { MovieDetailComponent } from './movies/movieDetail.component';

import { ProfileComponent } from './authAndProfile/profile.component';

const APPROUTES: Routes = [
  { path: '', redirectTo: '/shows', pathMatch: 'full' },
  { path: 'shows', component: ShowsComponent },
  { path: 'show/:id', component: ShowDetailComponent },

  { path: 'venues', component: VenueListComponent },
  { path: 'venue/:id', component: VenueDetailComponent },

  { path: 'movies', component: MovieListComponent },
  { path: 'movie/:id', component: MovieDetailComponent },

  { path: 'feedback', loadChildren: './feedback/feedback.module#FeedbackModule' },
  { path: 'info', loadChildren: './policies/policies.module#PoliciesModule' },
  //  { path: 'disclaimer', loadChildren: './policies/policies.module#PoliciesModule' },
  //  { path: 'privacypolicy', loadChildren: './policies/policies.module#PoliciesModule' },

  { path: 'profile', component: ProfileComponent, canActivate: [CanActivateViaAuthGuard] }
];

export const ROUTING_MODULE: ModuleWithProviders = RouterModule.forRoot(APPROUTES, {preloadingStrategy: PreloadAllModules});



// import { provideRouter, RouterConfig } from '@angular/router';




// export const AppRoutes: RouterConfig = [



// ];

// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(AppRoutes)
// ];