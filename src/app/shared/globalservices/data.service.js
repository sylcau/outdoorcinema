"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
//import 'rxjs/Rx';
// Statics
require('rxjs/add/observable/throw');
// Operators
require('rxjs/add/operator/catch');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/map');
require('rxjs/add/operator/mergeMap');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/do');
require('rxjs/add/operator/concat');
var auth_service_1 = require('./auth.service');
var DataService = (function () {
    function DataService(http, AuthService) {
        this.http = http;
        this.AuthService = AuthService;
        this.outcineUrl = ''; // URL to web api
        this.env = "http://localhost:5000";
        //private env = "http://www.myoutdoorcinema.com.au"
        this.urls = {
            shows: this.env + '/api/v1/getShows',
            venues: this.env + '/api/v1/getVenues',
            movies: this.env + '/api/v1/getMovies',
            movieByRef: this.env + '/api/v1/getMovieByRef',
            moviesbySeason: this.env + '/api/v1/getMoviesBySeason',
            getComments: this.env + '/api/v1/getComments',
            getMyComments: this.env + '/api/v1/getMyComments',
            addComment: this.env + '/api/v1/addComment',
            updateComment: this.env + '/api/v1/updateComment',
            deleteComment: this.env + '/api/v1/deleteComment',
            addReminder: this.env + '/api/v1/addReminder',
            getReminders: this.env + '/api/v1/getReminders',
            removeReminder: this.env + '/api/v1/removeReminders',
            postfeedback: this.env + '/api/v1/feedback'
        };
        this.day = new Date();
        this.day = new Date(2016, 1, 5); //Mock
        this.shows = [];
        this.venues = [];
        this.movies = [];
    }
    // *** SHOWS API ***
    DataService.prototype.getShows = function () {
        var _this = this;
        //return Promise.resolve(SHOWS);
        console.log("nb shows " + this.shows.length);
        if (!this.showsPromise) {
            //dev
            var params = new http_1.URLSearchParams();
            var d = this.day;
            params.set('today', d.toString());
            //
            this.showsPromise = this.http.get(this.urls.shows, { search: params })
                .toPromise()
                .then(this.extractData)
                .then(function (res) { return _this.shows = res; })
                .catch(this.handleError);
        }
        return this.showsPromise;
    };
    ;
    DataService.prototype.getShow = function (idRef) {
        var PromiseShows = this.getShows()
            .then(function (shows) {
            //console.log(shows);
            return shows.filter(function (h) { return h.idRef === idRef; })[0];
        });
        //.then(shows => shows.filter (h => h.movieId.movieRef === movieRef))
        var result = Promise.all([PromiseShows, this.getVenues()])
            .then(function (res) {
            var show = res[0]; // return a single object
            var venuesArr = res[1];
            show.venue = venuesArr.filter(function (v) { return v._id === show.venueId; })[0];
            console.log("show: ", show, show.venue);
            if (show.venue) {
                console.log(show.venue.cinema + " " + show.venue.suburb);
                show.venue.fullname = show.venue.cinema + " " + show.venue.suburb;
            }
            else {
                console.error("error: cannot find venue: " + show.venueId);
            }
            show.startDay_Date = new Date(show.startDay);
            return show;
        })
            .catch(function (err) { return console.log("getShow: ", err); });
        return result;
    };
    DataService.prototype.getShowsWithVenues = function () {
        var result = Promise.all([this.getShows(), this.getVenues()])
            .then(function (res) {
            res[0].forEach(function (show) {
                show.venue = res[1].filter(function (v) { return v._id === show.venueId; })[0];
            });
            return res[0];
        });
        return result;
    };
    //
    DataService.prototype.getShowsByVenueRef = function (venueRef) {
        //MOCK == TODOD
        //return Promise.resolve(SHOWS) 
        return Promise.all([this.getShows(), this.getVenues()])
            .then(function (res) {
            var venueId = res[1].filter(function (v) { return v.venueRef === venueRef; })[0]._id;
            var shows = res[0].filter(function (s) { return s.venueId === venueId; });
            shows.forEach(function (val) { return val.startDay_Date = new Date(val.startDay); });
            console.log('getShowsByVenueRef ' + venueId + " | nb Shows: " + shows.length);
            return shows;
        });
    };
    // getShowAndVenues(){
    //     this.
    // };
    // getShowVenues_1movie(){
    // };
    DataService.prototype.getShowsByMovieRef_SortedByDates_WithVenues = function (movieRef) {
        var _this = this;
        var PromiseShowsSorted = this.getShows()
            .then(function (shows) {
            return _this.sortShowsByDate(shows.filter(function (h) { return h.movieId.movieRef === movieRef; }));
        });
        //.then(shows => shows.filter (h => h.movieId.movieRef === movieRef))
        var result = Promise.all([PromiseShowsSorted, this.getVenues()])
            .then(function (res) {
            var showArr = res[0];
            var venuesArr = res[1];
            showArr.forEach(function (val) {
                /// TODO need to assign venues
                val.venue = venuesArr.filter(function (v) { return v._id === val.venueId; })[0];
                console.log(val.venue);
                if (val.venue) {
                    console.log(val.venue.cinema + " " + val.venue.suburb);
                    val.venue.fullname = val.venue.cinema + " " + val.venue.suburb;
                }
                else
                    console.error("error: cannot find venue: " + val.venueId);
                val.startDay_Date = new Date(val.startDay);
            });
            return showArr;
        });
        return result;
    };
    ;
    // *** MOVIES API ***     
    DataService.prototype.getMovies = function () {
        if (!this.moviesPromise) {
            this.moviesPromise = this.http.get(this.urls.moviesbySeason)
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);
        }
        return this.moviesPromise;
    };
    ;
    DataService.prototype.getMovieByRef = function (movieRef) {
        //MOCK
        //return Promise.resolve([GHOST])
        //.then(movies => movies.filter(m => m.movieRef === movieRef)[0]);
        //return Promise.resolve(GHOST)
        var params = new http_1.URLSearchParams();
        params.set('mvRef', movieRef);
        return this.http.get(this.urls.movieByRef, { search: params })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    ;
    // *** VENUES API ***
    DataService.prototype.getVenues = function () {
        var _this = this;
        //return Promise.resolve(VENUES);
        if (!this.venuesPromise) {
            this.venuesPromise = this.http.get(this.urls.venues)
                .toPromise()
                .then(this.extractData)
                .then(function (res) { return _this.venues = res; })
                .catch(this.handleError);
        }
        return this.venuesPromise;
    };
    ;
    DataService.prototype.getVenueByRef = function (venueRef) {
        return this.getVenues()
            .then(function (venues) {
            console.log('data0: ' + venueRef);
            return venues.filter(function (v) { return v.venueRef === venueRef; })[0];
        });
    };
    ;
    DataService.prototype.getVenuesGeoLocation = function () {
        return this.getVenues()
            .then(function (res) {
            return res;
        });
    };
    ;
    DataService.prototype.getMapCenter = function () {
        return {
            lat: -31.9546529,
            long: 115.852662
        };
    };
    // *** Utils API ***    
    DataService.prototype.getDay = function () {
        //return Promise.resolve(this.day);
        //Mock
        return this.day;
    };
    ;
    DataService.prototype.setDay = function (dateObj) {
        this.day = dateObj;
        return Promise.resolve(this.day);
    };
    ;
    DataService.prototype.addDay = function (day, i) {
        //console.log(typeof day + " |" + day)
        day.setDate(this.day.getDate() + i);
        var d = new Date(day.toUTCString());
        this.day = d;
        return d;
    };
    ;
    DataService.prototype.sortShowsByDate = function (arr) {
        return arr.sort(function (a, b) {
            var ad = new Date(a.startDay);
            var bd = new Date(b.startDay);
            //return new Date(a.startDay) - new Date(b.startDay); 
            return ad > bd ? -1 : ad < bd ? 1 : 0;
        });
    };
    ;
    // *** COMMENTS ***
    DataService.prototype.getComments = function (parentId) {
        var params = new http_1.URLSearchParams();
        params.set('parentId', parentId);
        return this.http.get(this.urls.getComments, { search: params })
            .map(this.extractData)
            .catch(this.handleErrorOb);
    };
    ;
    DataService.prototype.getMyComments = function (userId) {
        var params = new http_1.URLSearchParams();
        params.set('userId', userId);
        return this.http.get(this.urls.getMyComments, { search: params })
            .map(this.extractDataOb)
            .catch(this.handleErrorOb);
    };
    ;
    DataService.prototype.addComment = function (comment) {
        var _this = this;
        var token = this.AuthService.getToken();
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this.AuthService.getUser().flatMap(function (res) {
            comment.user = res._id;
            var body = JSON.stringify(comment);
            console.log(body);
            //
            return _this.http.post(_this.urls.addComment, body, options)
                .map(_this.extractDataOb)
                .catch(_this.handleErrorOb);
        });
    };
    ;
    DataService.prototype.updateComment = function (comment) {
        var _this = this;
        var token = this.AuthService.getToken();
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this.AuthService.getUser().flatMap(function (res) {
            comment.user = res._id;
            var body = JSON.stringify(comment);
            console.log(body);
            //
            return _this.http.post(_this.urls.updateComment, body, options)
                .map(_this.extractDataOb)
                .catch(_this.handleErrorOb);
        });
    };
    ;
    DataService.prototype.deleteComment = function (comment) {
        var _this = this;
        console.log("deleteComment service");
        var token = this.AuthService.getToken();
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this.AuthService.getUser().flatMap(function (res) {
            comment.user = res._id;
            var body = JSON.stringify(comment);
            console.log(body);
            //
            return _this.http.post(_this.urls.deleteComment, body, options)
                .map(_this.extractDataOb)
                .catch(_this.handleErrorOb);
        });
    };
    ;
    // FEEDBACK
    DataService.prototype.sendFeedback = function (feedback) {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify(feedback);
        return this.http.post(this.urls.postfeedback, body, options)
            .map(this.extractDataOb)
            .catch(this.handleErrorOb);
    };
    // http handle Promise:
    DataService.prototype.extractData = function (res) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        var body = res.json();
        return body.data || [];
    };
    DataService.prototype.handleError = function (error) {
        // In a real world app, we might send the error to remote logging infrastructure
        var errMsg = error.message || 'Server error';
        console.log(errMsg); // log to console instead
        return Promise.reject(errMsg);
    };
    DataService.prototype.extractDataOb = function (res) {
        var body = res.json();
        console.log(body.data);
        return body.data || {};
    };
    DataService.prototype.handleErrorOb = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable_1.Observable.throw(errMsg);
    };
    DataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, auth_service_1.AuthService])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
// addHero (name: string): Promise<Hero> {
//   let body = JSON.stringify({ name });
//   let headers = new Headers({ 'Content-Type': 'application/json' });
//   let options = new RequestOptions({ headers: headers });
//   return this.http.post(this.heroesUrl, body, options)
//              .toPromise()
//              .then(this.extractData)
//              .catch(this.handleError);
// }
//# sourceMappingURL=data.service.js.map