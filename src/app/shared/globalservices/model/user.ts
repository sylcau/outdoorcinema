import { Show } from './show';

export interface User {
  __v?: number;
  _id: string;
  email: string;
  displayName: string;
  picture: string;

  //extra
  reminders?: Reminder[];
};

interface ObjectId {
  _id:string;
}

export interface Reminder {
  _id?: string;
  show: ObjectId | Show;
  nbDays: number;
  sentDate?: string;
}
