import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap/ng2-bootstrap';
//import { AuthService }            from 'ng2-ui-auth'; //from '../_nm/ng2-ui-auth/src/auth.service';

import {
  AuthenticationService } from '../shared/index';


@Component({
  // moduleId: module.id,
  selector: 'login-modal',
  styleUrls: ['./loginModal.component.css'],
  templateUrl: './loginModal.component.html',
  exportAs: 'child'
})

export class LoginModalComponent implements OnInit, OnDestroy {
  @ViewChild('childModal') childModal: ModalDirective;

  // listen for loginModal require
  public submitted: boolean = false;
  private sub:any;
  //@Output() isLogin : EventEmitter<any> = new EventEmitter();

  constructor(
    private authenticationService: AuthenticationService
    ) {
    this.sub = authenticationService.isLoginModalRequired$.subscribe(
      val => {
        if (val) this.show();
      });
    }

  ngOnInit() { }

  public show(): void {
    this.childModal.config.backdrop = false; // workaround issue https://github.com/valor-software/ng2-bootstrap/issues/1235
    this.childModal.show();
  }

  public hide(): void {
    this.childModal.hide();
  }

  login(provider: string) {
    this.submitted = true;
    this.authenticationService.authenticate(provider).subscribe(() => {
      this.submitted = false;
      this.childModal.hide();
    },
    err => console.log(err),
    () => this.submitted = false);
  }

  ngOnDestroy() {
    if (this.sub !== null) {
      this.sub.unsubscribe();
    }
  }
}
