import { Routes } from '@angular/router';

// import { AboutRoutes } from './about/index';
// import { HomeRoutes } from './home/index';

export const routes: Routes = [
  {
    path: '',
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: 'about',
    loadChildren: './about/about.module#AboutModule'
  }
  // ...HomeRoutes,
  // ...AboutRoutes
];
// http://stackoverflow.com/questions/39493782/lazy-loading-in-angular2-rc7-and-angular-cli-webpack