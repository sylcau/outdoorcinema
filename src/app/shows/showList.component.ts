import { Component, OnInit, Input } from '@angular/core';

import {
  Show } from '../shared/globalservices/index';


import { Router } from '@angular/router';

@Component({
  // moduleId: module.id,
  selector: 'show-list',
  templateUrl: './showList.component.html',
  styleUrls: ['./showList.component.css']

})

export class ShowListComponent implements OnInit {

  @Input() public shows: Show[] = [];
  @Input() public isLoaded: any = false;
  @Input() public noResultsText: string = 'No Results Found';
  @Input() public showDate: boolean;
  @Input() public reduceTpl: boolean;
  @Input() public showsComingUp: Show[] = [];


  // private show: Show;
  public dateCheck: boolean = false;

  constructor(
    private _router: Router) {
  }

  ngOnInit() {
    if (!this.shows) {
      this.shows = [];
    }
    let d = new Date();
    let opening = new Date(2016, 9, 20);
    if ( opening.getTime() > d.getTime() ) {
      this.dateCheck = true;
    }
  }

  gotoDetail(show: Show) {
    let link = ['/show', show.idRef];
    this._router.navigate(link);
  }

}

// --- for first letter Uppercase
// function hasCapitals(str) {
//     var hasCapitals = str.match(/[A-Z]/);
//     return hasCapitals !== null;
// }
// function titlecaseLowercaseWords(str) {
//     if (hasCapitals(str)) {
//         return str;
//     }
//     var firstLetterIndex = str.match(/[a-z]/).index,
//         strParts = str.split('');
//     strParts[firstLetterIndex] = str[firstLetterIndex].toUpperCase();
//     return strParts.join('');
// }
// function titleCase(str) {
//     var wordsRx = /(^|\s)\w*/g;
//     return str.replace(wordsRx, titlecaseLowercaseWords);
// }
// var result = titleCase("the USA's involvement with NASA in conjunction with eBay and iTunes.");
// // "The USA's Involvement With NASA In Conjunction With eBay And iTunes."



            // <li *ngIf="reduceTpl" *ngFor="let show of shows" (click)="gotoDetail(show)" class="shadowbox"> 
            //         <div *ngIf="showDate" class="showDate" > {{show.startDay_Date | date: "fullDate" }}  </div>
            //         <p>{{show?.venue.cinema}} {{show?.venue.suburb}}  </p>
            //         <p>Movie starts: {{show.movieStart}} </p>
            // </li>
