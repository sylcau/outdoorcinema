"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var ShowListComponent = (function () {
    function ShowListComponent(_router) {
        this._router = _router;
        this.shows = [];
        this.isLoaded = false;
        this.noResultsText = "No Results Found";
    }
    ShowListComponent.prototype.ngOnInit = function () {
        if (!this.shows) {
            this.shows = [];
        }
    };
    ShowListComponent.prototype.gotoDetail = function (show) {
        var link = ['/show', show.idRef];
        this._router.navigate(link);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], ShowListComponent.prototype, "shows", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ShowListComponent.prototype, "isLoaded", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], ShowListComponent.prototype, "noResultsText", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ShowListComponent.prototype, "showDate", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], ShowListComponent.prototype, "reduceTpl", void 0);
    ShowListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'show-list',
            template: "\n    <spinner [isHidding]=\"isLoaded\"> </spinner>\n        <ul class=\"shows\" *ngIf=\"isLoaded && !reduceTpl\">\n            <li *ngFor=\"let show of shows\" (click)=\"gotoDetail(show)\" class=\"shadowbox col-md-6\"> \n                    <div *ngIf=\"showDate\" class=\"showDate\" > {{show.startDay_Date | date: \"fullDate\" }}  </div>\n                    <img src=\"{{show.movieId.img || 'img/movie.jpg'}}\" class=\"img-thumbnail\" style=\"max-height: 120px; width: 80px; vertical-align: top;\">\n                    <div style=\"text-align:left; display:inline-block;\" class=\"\">\n                        <span class=\"movieTitle\" style=\"\">{{show.movieId.movieTitle | uppercase }} </span>\n                        <p *ngIf=\"!showDate\" >{{show.venue.cinema}} {{show.venue.suburb}}  </p>\n                        <p>Movie starts: {{show.movieStart}} </p>\n                        <p class=\"runTime\">{{show.movieId.Runtime}} <span *ngIf=\"show.movieId.parentalRating\">({{show.movieId.parentalRating}}) </span> <br> {{show.movieId.Genre}}</p> \n                    </div>           \n            </li>\n        </ul>\n        <ul class=\"shows\" *ngIf=\"isLoaded && reduceTpl\">\n          <li *ngFor=\"let show of shows\" (click)=\"gotoDetail(show)\" class=\"shadowbox\"> \n            <div class=\"showDate\" > {{show.startDay_Date | date: \"fullDate\" }}  </div>\n            <p>{{show?.venue.cinema}} {{show?.venue.suburb}}  </p>\n            <p>Movie starts: {{show.movieStart}} </p>      \n          </li>\n        </ul>      \n       <alert *ngIf=\"shows.length == 0 && isLoaded\" [type]='\"info\"' style=\"font-weight: 600;\"> {{ noResultsText}} </alert> \n  ",
            styleUrls: ['./showList.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], ShowListComponent);
    return ShowListComponent;
}());
exports.ShowListComponent = ShowListComponent;
// --- for first letter Uppercase
// function hasCapitals(str) {
//     var hasCapitals = str.match(/[A-Z]/);
//     return hasCapitals !== null;
// }
// function titlecaseLowercaseWords(str) {
//     if (hasCapitals(str)) {
//         return str;
//     }
//     var firstLetterIndex = str.match(/[a-z]/).index,
//         strParts = str.split('');
//     strParts[firstLetterIndex] = str[firstLetterIndex].toUpperCase();
//     return strParts.join('');
// }
// function titleCase(str) {
//     var wordsRx = /(^|\s)\w*/g;
//     return str.replace(wordsRx, titlecaseLowercaseWords);
// }
// var result = titleCase("the USA's involvement with NASA in conjunction with eBay and iTunes.");
// // "The USA's Involvement With NASA In Conjunction With eBay And iTunes."
// <li *ngIf="reduceTpl" *ngFor="let show of shows" (click)="gotoDetail(show)" class="shadowbox"> 
//         <div *ngIf="showDate" class="showDate" > {{show.startDay_Date | date: "fullDate" }}  </div>
//         <p>{{show?.venue.cinema}} {{show?.venue.suburb}}  </p>
//         <p>Movie starts: {{show.movieStart}} </p>
// </li> 
//# sourceMappingURL=showList.component.js.map