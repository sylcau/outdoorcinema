"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var GoogleAdsenseComponent = (function () {
    function GoogleAdsenseComponent(w, d) {
        this._isLoaded = false;
        this._window = w;
        this._document = d;
    }
    GoogleAdsenseComponent.prototype.ngOnInit = function () {
        this.loadAdsense();
    };
    GoogleAdsenseComponent.prototype.loadAdsense = function () {
        var _this = this;
        console.log("loading Adsense...");
        if (this._isLoaded) {
            return; //this._isLoaded;
        }
        var script = this._document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.src = '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js';
        this._document.body.appendChild(script);
        //((<any>this._window).adsbygoogle = (<any>this._window).adsbygoogle || []).push({});
        //setTimeout(function(){(adsbygoogle = window.adsbygoogle || []).push({})}, 1000);
        setTimeout(function () {
            (_this._window.adsbygoogle = _this._window.adsbygoogle || []).push({});
        }, 1000);
        this._isLoaded = true;
        return;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], GoogleAdsenseComponent.prototype, "adClient", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], GoogleAdsenseComponent.prototype, "adSlot", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], GoogleAdsenseComponent.prototype, "inlineStyle", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], GoogleAdsenseComponent.prototype, "adFormat", void 0);
    GoogleAdsenseComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'goAdsense',
            template: "\n   <div class=\"ads\" \n      style=\"border-color: grey;\n      border-style: solid;\n      border-width: thin;\">\n\n      <ins class=\"adsbygoogle\" \n        data-ad-client=\"ca-pub-7004086848973531\"\n        data-ad-slot=\"5564204807\" \n        style=\"display:block;\" \n        data-ad-format=\"auto\"\n        >\n      </ins>\n   </div>"
        }), 
        __metadata('design:paramtypes', [Window, Document])
    ], GoogleAdsenseComponent);
    return GoogleAdsenseComponent;
}());
exports.GoogleAdsenseComponent = GoogleAdsenseComponent;
//   if (!Adsense.isAlreadyLoaded) {
//                 var s = document.createElement('script');
//                 s.type = 'text/javascript';
//                 s.src = Adsense.url;
//                 s.async = true;
//                 document.body.appendChild(s);
//                 Adsense.isAlreadyLoaded = true;
//             }
//             /**
//              * We need to wrap the call the AdSense in a $apply to update the bindings.
//              * Otherwise, we get a 400 error because AdSense gets literal strings from the directive
//              */
//             $timeout(function(){
//                  (window.adsbygoogle = window.adsbygoogle || []).push({});
//             }); 
//# sourceMappingURL=googleAdsense.component.js.map