import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable }     from 'rxjs/Observable';

import { CONFIG_API } from '../shared/index';
import { Feedback } from './feedback.model';

@Injectable()
export class FeedbackService {


  private urls = CONFIG_API.URLS;

  constructor(
    private http: Http
  ) {
  }

  // FEEDBACK
  sendFeedback(feedback: Feedback) {
    let headers = new Headers({ 'Content-Type': 'application/json' });

    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(feedback);

    return this.http.post(this.urls.postfeedback, body, options)
      .map(this.extractDataOb)
      .catch(this.handleErrorOb);
  }

  private extractDataOb(res: Response) {
    let body = res.json();
    console.log(body.data);
    return body.data || {};
  }
  private handleErrorOb(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


}

