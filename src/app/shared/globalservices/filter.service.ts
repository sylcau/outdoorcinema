import { Show } from '../index';

export class FilterService {
  constructor() { }

  filter(data: string, props: Array<string>, originalList: Array<any>, maxNb: number) {
    let filteredList: any[];
    // assumed originalList Array already sorted!
    if (data && props && originalList && maxNb) {
      data = data.toLowerCase();
      let filtered = originalList.filter(item => {
        let match = false;
        for (let prop of props) {
          if (item[prop].toString().toLowerCase().indexOf(data) > -1) {
            match = true;
            break;
          }
        };
        return match;
      });
      filteredList = filtered.slice(0, maxNb);

    } else if (originalList && maxNb) {
      filteredList = originalList.slice(0, maxNb);

    } else {
      filteredList = originalList;

    }
    return filteredList;
  };

  filterDateVenue(date: Date, prop: string, originalList: Array<any>, param2: string, prop2: string) {
    let filteredList: any[];
    if (date && prop && originalList && param2 && prop2) {
      // let dateObj = new Date(date);
      let filtered = originalList.filter(item => {

        let startDayObj = new Date(item[prop]);
        let startDay = startDayObj.getFullYear() + '-' + startDayObj.getMonth() + '-' + startDayObj.getDate();
        let todayStr = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();

        let bool = false;
        if (param2 === '0' || item[prop2] === param2) { bool = true; }

        if (startDay === todayStr && bool) {
          return true;
        }
        else {
          return false;
        }

      });
      filteredList = filtered;
    }
    else {
      filteredList = originalList;
    }

    return filteredList;
  };

  filterComingUp(date: Date, prop: string, originalList: Array<Show>) {
    let filteredList: Array<Show>;
    // if (date.getTime() > today.getTime()) retinr [];

    if (date && prop && originalList) {
      // let dateObj = new Date(date);

      // copy array
      filteredList = originalList.slice(0);

      // sort array by date
      filteredList.sort( (a: Show , b: Show) => {
        let aStartDayObj = new Date(a.startDay);
        let bStartDayObj = new Date(b.startDay);
        return aStartDayObj.getTime() - bStartDayObj.getTime();
      });
      filteredList = filteredList.slice(0, 4);
      filteredList.forEach(function(s){
        if (typeof s.startDay_Date === 'undefined' ) {
          s.startDay_Date = new Date(s.startDay);
        }
      });
    } else {
      filteredList = [];
    }

    return filteredList;
  }
}
