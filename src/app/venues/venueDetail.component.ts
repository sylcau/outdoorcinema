import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';

import {
  DataService,
  Show, Venue } from '../shared/globalservices/index';



@Component({
  // moduleId: module.id,

  selector: 'venue-detail',
  templateUrl: './venueDetail.component.html',

  styleUrls: ['./venueDetail.component.css']
})

export class VenueDetailComponent implements OnInit, OnDestroy {
  public venue: Venue;
  private shows: Show[];
  //private curSegment: RouteSegment;

  private mapCenterLat: number;
  private mapCenterLong: number;

  private sub: any;

  constructor(
    private _dataService: DataService,
    private _router: Router,
    private route: ActivatedRoute) {

  }

  // routerOnActivate(curr: RouteSegment): void {
  //   this.curSegment = curr;

  //   let id = curr.getParam('id');


  // }
  ngOnInit() {
    this.sub = this.route.params
      .map(params => params['id'])
      .subscribe((id) => {
        this._dataService.getVenueByRef(id)
          .then(venue => this.venue = venue)
          .catch(err => console.log('error server'));

        this.getShows(id);
        this.getPerthGeoLocation();
      });
  }

  goBack() {
    window.history.back();
  }

  getShows(venueRef: string) {
    this._dataService.getShowsByVenueRef(venueRef)
    .then(s => this.shows = s)
    .catch(err => console.log('error server'));

  }

  getShow(id:string) {
    let link = ['/show', id];
    this._router.navigate(link);
  }

  getPerthGeoLocation() {
    this.mapCenterLat = this._dataService.getMapCenter().lat;
    this.mapCenterLong = this._dataService.getMapCenter().long;
  }

  ngOnDestroy() {
    console.log('venueDetail deleted')
    if (this.sub !== null) this.sub.unsubscribe();
  }
}
