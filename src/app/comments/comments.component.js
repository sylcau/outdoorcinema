"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var utils_service_1 = require('../Aservice/utils.service');
var data_service_1 = require('../Aservice/data.service');
var auth_service_1 = require('../Aservice/auth.service');
var CommentsComponent = (function () {
    function CommentsComponent(UtilsService, DataService, Auth) {
        this.UtilsService = UtilsService;
        this.DataService = DataService;
        this.Auth = Auth;
        //private btnEditTxt: string = "edit";
        this.myComments = false;
        this.parentId = null;
    }
    CommentsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.isAuthenticated()) {
            this.Auth.getUser().subscribe(function (user) {
                if (_this.myComments) {
                    _this.getMyComments(user._id);
                }
            });
        }
        if (this.parentId) {
            this.getComments(this.parentId);
        }
    };
    ;
    CommentsComponent.prototype.isAuthenticated = function () {
        return this.Auth.isAuthenticated();
    };
    CommentsComponent.prototype.timeAgo = function (date) {
        return this.UtilsService.timeAgo(date);
    };
    CommentsComponent.prototype.getComments = function (parentId) {
        var _this = this;
        this.DataService.getComments(parentId)
            .subscribe(function (comments) { return _this.comments = comments; }, function (error) { return _this.errorMessage = error; });
    };
    CommentsComponent.prototype.getMyComments = function (userId) {
        var _this = this;
        this.DataService.getMyComments(userId)
            .subscribe(function (comments) { return _this.comments = comments; }, function (error) { return _this.errorMessage = error; });
    };
    CommentsComponent.prototype.isMe = function (userId) {
        return this.Auth.isMe(userId);
    };
    CommentsComponent.prototype.addComment = function () {
        var _this = this;
        console.log(this.newCommentText);
        var comment = {
            parentId: this.parentId,
            text: this.newCommentText,
            user: ""
        };
        console.log(comment);
        this.DataService.addComment(comment).subscribe(function (res) {
            console.log(res);
            _this.newCommentText = undefined;
            _this.getComments(_this.parentId);
        });
    };
    CommentsComponent.prototype.editComment = function (comment) {
        this.comments.filter(function (c) { return c._id === comment._id; })[0].isEdited = true;
        // initialise new text //if not already modified.
        //console.log(typeof comment.newText,comment.newText)
        //if (typeof comment.newText !== "string") {
        comment.newText = comment.text;
        //}
    };
    CommentsComponent.prototype.editUpdate = function (comment) {
        var _this = this;
        comment.text = comment.newText;
        console.log(comment);
        this.DataService.updateComment(comment).subscribe(function (res) {
            console.log(res);
            _this.getComments(_this.parentId);
        });
    };
    ;
    CommentsComponent.prototype.editClose = function (comment) {
        comment.isEdited = false;
    };
    CommentsComponent.prototype.editDelete = function (comment) {
        var _this = this;
        comment.isEdited = false;
        //spinner or remove from array???
        this.DataService.deleteComment(comment).subscribe(function (res) {
            console.log(res);
            _this.getComments(_this.parentId);
        });
    };
    CommentsComponent.prototype.btnEditText = function (comment) {
        if (comment.isEdited) {
            return "Save";
        }
        else {
            return "Edit";
        }
    };
    CommentsComponent.prototype.disabled = function (newCommentText) {
        if (typeof newCommentText !== "string" || newCommentText.length === 0)
            return true;
        return false;
    };
    CommentsComponent.prototype.login = function () {
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], CommentsComponent.prototype, "myComments", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CommentsComponent.prototype, "parentId", void 0);
    CommentsComponent = __decorate([
        core_1.Component({
            //moduleId: module.id,
            selector: 'comments',
            templateUrl: 'app/comments/comments.component.html',
            styleUrls: ['app/comments/comments.component.css'] //,
        }), 
        __metadata('design:paramtypes', [utils_service_1.UtilsService, data_service_1.DataService, auth_service_1.AuthService])
    ], CommentsComponent);
    return CommentsComponent;
}());
exports.CommentsComponent = CommentsComponent;
//# sourceMappingURL=comments.component.js.map