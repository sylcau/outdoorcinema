"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
//import {ProtectedDirective}      from "./protected.directive";
var auth_service_1 = require('../Aservice/auth.service');
var router_1 = require('@angular/router');
var ProfileComponent = (function () {
    function ProfileComponent(AuthService, router) {
        this.AuthService = AuthService;
        this.router = router;
        this.oneAtATime = true;
        this.status = {
            isFirstOpen: true,
            isFirstDisabled: false,
            isOpen: false
        };
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.AuthService.isAuthenticated()) {
            this.router.navigate(['/shows']);
            return;
        }
        this.AuthService.getUser().subscribe(function (res) { return _this.user = res; });
    };
    ProfileComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'profile',
            template: '',
            //templateUrl: './profile.component.html',
            styleUrls: ['./profile.component.css'] //,
        }), 
        __metadata('design:paramtypes', [auth_service_1.AuthService, router_1.Router])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map