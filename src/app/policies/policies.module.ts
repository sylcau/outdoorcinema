import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PrivacyPolicyComponent, DisclaimerComponent, AboutUsComponent } from './index';

import { RouterModule } from '@angular/router';

import { POLICIESROUTES } from './policies.routes';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(POLICIESROUTES)],
  declarations: [PrivacyPolicyComponent, DisclaimerComponent, AboutUsComponent],
  exports: [PrivacyPolicyComponent, DisclaimerComponent, AboutUsComponent],
  providers: []
})
export class PoliciesModule { }
