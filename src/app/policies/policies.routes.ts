import { Route } from '@angular/router';
import { PrivacyPolicyComponent, DisclaimerComponent, AboutUsComponent } from './index';

export const POLICIESROUTES: Route[] = [
  {
    path: 'privacypolicy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'disclaimer',
    component: DisclaimerComponent
  },
  {
    path: 'aboutus',
    component: AboutUsComponent
  }

];
