"use strict";
var FilterService = (function () {
    function FilterService() {
    }
    FilterService.prototype.filter = function (data, props, originalList, maxNb) {
        var filteredList;
        if (data && props && originalList && maxNb) {
            data = data.toLowerCase();
            var filtered = originalList.filter(function (item) {
                var match = false;
                for (var _i = 0, props_1 = props; _i < props_1.length; _i++) {
                    var prop = props_1[_i];
                    if (item[prop].toString().toLowerCase().indexOf(data) > -1) {
                        match = true;
                        break;
                    }
                }
                ;
                return match;
            });
            filteredList = filtered.slice(0, maxNb);
        }
        else if (originalList && maxNb) {
            filteredList = originalList.slice(0, maxNb);
        }
        else {
            filteredList = originalList;
        }
        return filteredList;
    };
    FilterService.prototype.filterDateVenue = function (date, prop, originalList, param2, prop2) {
        var filteredList;
        if (date && prop && originalList && param2 && prop2) {
            //let dateObj = new Date(date);
            var filtered = originalList.filter(function (item) {
                var startDayObj = new Date(item[prop]);
                var startDay = startDayObj.getFullYear() + "-" + startDayObj.getMonth() + "-" + startDayObj.getDate();
                var todayStr = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                var bool = false;
                if (param2 === "0" || item[prop2] === param2) {
                    bool = true;
                }
                if (startDay === todayStr && bool) {
                    return true;
                }
                else {
                    return false;
                }
            });
            filteredList = filtered;
        }
        else {
            filteredList = originalList;
        }
        return filteredList;
    };
    return FilterService;
}());
exports.FilterService = FilterService;
//# sourceMappingURL=filter.service.js.map