/**
 * This barrel file provides the export for the lazy loaded FeedbackComponent.
 */
export * from './feedback.component';
export * from './feedback.routes';
