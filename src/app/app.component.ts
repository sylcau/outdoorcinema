import { Component, OnInit, OnDestroy, HostListener, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import {
  AuthenticationService, StripeService,
  CONFIG_API } from './shared/index';

@Component({
  // // moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {


  //private title = 'My Outdoor Cinema';
  private link: string;
  public isCollapsed = true;
  private isAtTheTop = true;
  private isShow = false;
  public isShow_isAtTheTop = false;
  private sub: any;
  private url: [any];
  public isAuthenticated: boolean = false;
  private viewContainerRef: ViewContainerRef;;

  constructor(
    private stripeService: StripeService,
    public route: ActivatedRoute,
    private router: Router,
    private auth: AuthenticationService,
    viewContainerRef: ViewContainerRef
  ) {
    this.viewContainerRef = viewContainerRef;
    auth.isLoggedIn$.subscribe(
      val => {
        // console.log(val)
        this.isAuthenticated = val;
      });
  }

  ngOnInit() {
    // detect if page is shows to add transparent nav class
    this.sub = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let url = this.router.url;
        console.log(this.router.url);

        if (url === '/' || url === '/shows') {
          this.isShow = true;
          console.log(this.isShow);

        } else {
          this.isShow = false;
        }

        if (this.isShow && this.isAtTheTop) {
          this.isShow_isAtTheTop = true;
        } else {
          this.isShow_isAtTheTop = false;
        }

        window.scrollTo(0, 0);
      }
    });

    // stripe hack http://stackoverflow.com/questions/36258252/stripe-json-circular-reference
    const _stringify = <any>JSON.stringify;
    JSON.stringify = function (value: any, ...args: any[]) {
      if (args.length) {
        return _stringify(value, ...args);
      } else {
        return _stringify(value, function (key: any, value: any) {
          if (value && key === 'zone' && value['_zoneDelegate']
            && value['_zoneDelegate']['zone'] === value) {
            return undefined;
          }
          return value;
        });
      }
    };

    this.isAuthenticated = this.auth.isAuthenticated();

  }


  openCheckout() {
    let that = this;
    let handler = (<any>window).StripeCheckout.configure({
      key: CONFIG_API.APIkeys.stripe,
      locale: 'auto',
      token: function (token: any) {
        // You can access the token ID with `token.id`.
        console.log(token.id);
        // Get the token ID to your server-side code for use.
        console.log(that.stripeService.sendToken);
        that.stripeService.sendToken(token).subscribe();
      }
    });

    handler.open({
      name: 'My Outdoor Cinema',
      description: 'thanks for supporting my outdoor cinema!',
      amount: 1000
    });
  }

  // detect scroll to modify nav background
  @HostListener('window:scroll', ['$event'])
  checkLocation(event: any) {
    // console.debug("Scroll Event", document.body.scrollTop);
    if (document.body.scrollTop < 70) {
      // console.debug("Scroll Event", document.body.scrollTop);            
      this.isAtTheTop = true;
    } else {
      this.isAtTheTop = false;
    }

    if (this.isShow && this.isAtTheTop) {
      this.isShow_isAtTheTop = true;
    } else {
      this.isShow_isAtTheTop = false;
    }

  }

  signout() {
    this.auth.logout().subscribe(() => {
      // this.isAuthenticated = this.auth.isAuthenticated();
      this.router.navigate(['/shows']);
    });
  }

  ngOnDestroy() {
    if (this.sub !== null) {
      this.sub.unsubscribe();
    }
  }


}