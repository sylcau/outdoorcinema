import { Component, OnInit, OnDestroy } from '@angular/core';
//import { Router } from '@angular/router';
//import { NgForm } from '@angular/forms';

import { FeedbackService } from './feedback.service';

import { Feedback } from './feedback.model';



@Component({
  // moduleId: module.id,
  selector: 'feedback',
  //template: '',
  templateUrl:'./feedback.component.html',

  styles: [`

  .ng-valid[required] {
    border-left: 5px solid #42A948; /* green */
  }
  .ng-invalid:not(form) {
    border-left: 5px solid #a94442; /* red */
  }

  `]
})
export class FeedbackComponent implements OnInit, OnDestroy {

  submitted = false;
  model = new Feedback('', '');
  successSend: boolean = false;
  errorSend: boolean = false;
  active = true;
  sub:any =null;

  constructor(
    private FeedbackService: FeedbackService
  ) {

  }

  ngOnInit() {
    // TODO: If login In email default should the registered email.
  }

  onSubmit() {
    this.submitted = true; //disable the fields & activate spinner
    //this.active = false;

    this.sub = this.FeedbackService.sendFeedback(this.model)
      .subscribe(
      res => {
        console.log('successfully send feedback');
        this.submitted = false;
        this.successSend = true; // alert message
        this.model.text = '';
        //this.model.email = null;

      },
      error => {
        console.log('error');
        this.submitted = false;
        this.errorSend = true;
      });

  }

  resetAlert() {
    console.log('triggered');
    this.successSend = false;
    this.errorSend = false;
  }

  //get diagnostic() { return JSON.stringify(this.model); }
  ngOnDestroy() {
    if (this.sub !== null) this.sub.unsubscribe();
  }
}
