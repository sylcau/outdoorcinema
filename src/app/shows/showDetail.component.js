"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
//import {RouteParams, ROUTER_DIRECTIVES, RouteConfig} from '@angular/router-deprecated';
var router_1 = require('@angular/router');
var data_service_1 = require('../Aservice/data.service');
var ShowDetailComponent = (function () {
    function ShowDetailComponent(dataService, router, route) {
        this.dataService = dataService;
        this.router = router;
        this.route = route;
    }
    ShowDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .map(function (params) { return params['id']; })
            .subscribe(function (id) {
            console.log(typeof id, parseInt(id));
            _this.dataService.getShow(parseInt(id))
                .then(function (show) {
                _this.show = show;
                //console.log(show)
            })
                .catch(function (err) { return console.log("error server: ", err); });
        });
    };
    ShowDetailComponent.prototype.goToVenueDetail = function (venueRef) {
        var link = ['/venue', venueRef];
        this.router.navigate(link);
    };
    ShowDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'showDetail',
            templateUrl: './showDetail.component.html',
            styleUrls: ["./showDetail.component.css"]
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, router_1.Router, router_1.ActivatedRoute])
    ], ShowDetailComponent);
    return ShowDetailComponent;
}());
exports.ShowDetailComponent = ShowDetailComponent;
//# sourceMappingURL=showDetail.component.js.map