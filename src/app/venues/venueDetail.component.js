"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var data_service_1 = require('../Aservice/data.service');
var VenueDetailComponent = (function () {
    function VenueDetailComponent(_dataService, _router, route) {
        this._dataService = _dataService;
        this._router = _router;
        this.route = route;
    }
    // routerOnActivate(curr: RouteSegment): void {
    //   this.curSegment = curr;
    //   let id = curr.getParam('id');
    // }
    VenueDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .map(function (params) { return params['id']; })
            .subscribe(function (id) {
            _this._dataService.getVenueByRef(id)
                .then(function (venue) { return _this.venue = venue; })
                .catch(function (err) { return console.log("error server"); });
            _this.getShows(id);
            _this.getPerthGeoLocation();
        });
    };
    VenueDetailComponent.prototype.goBack = function () {
        window.history.back();
    };
    VenueDetailComponent.prototype.getShows = function (venueRef) {
        var _this = this;
        this._dataService.getShowsByVenueRef(venueRef).then(function (s) { return _this.shows = s; })
            .catch(function (err) { return console.log("error server"); });
    };
    VenueDetailComponent.prototype.getShow = function (id) {
        var link = ['/show', id];
        this._router.navigate(link);
    };
    VenueDetailComponent.prototype.getPerthGeoLocation = function () {
        this.mapCenterLat = this._dataService.getMapCenter().lat;
        this.mapCenterLong = this._dataService.getMapCenter().long;
    };
    VenueDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'venueDetail',
            templateUrl: './venueDetail.component.html',
            styleUrls: ['./venueDetail.component.css'],
            styles: ["\n    .sebm-google-map-container {\n      height: 300px;\n    }\n  "]
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, router_1.Router, router_1.ActivatedRoute])
    ], VenueDetailComponent);
    return VenueDetailComponent;
}());
exports.VenueDetailComponent = VenueDetailComponent;
//# sourceMappingURL=venueDetail.component.js.map