import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
//import { AuthService } from 'ng2-ui-auth'; // from '../_nm/ng2-ui-auth/src/auth.service'

import {
  AuthenticationService } from '../shared/index';

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

  constructor(private authService: AuthenticationService) {}

  canActivate() {
    console.log('auth: ', this.authService.isAuthenticated())
    return this.authService.isAuthenticated();
  }
}
