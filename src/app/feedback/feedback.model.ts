export class Feedback {

    constructor(
      public email: string,
      public text: string
   ) {  }

}
