"use strict";
var UtilsService = (function () {
    function UtilsService() {
    }
    UtilsService.prototype.timeAgo = function (timestamp) {
        var units = [
            { name: "second", limit: 60, in_seconds: 1 },
            { name: "minute", limit: 3600, in_seconds: 60 },
            { name: "hour", limit: 86400, in_seconds: 3600 },
            { name: "day", limit: 604800, in_seconds: 86400 },
            { name: "week", limit: 2629743, in_seconds: 604800 },
            { name: "month", limit: 31556926, in_seconds: 2629743 },
            { name: "year", limit: null, in_seconds: 31556926 }
        ];
        var now = new Date();
        var diff = (now.getTime() - new Date(timestamp).getTime()) / 1000; //diff is number of second delay. 
        //console.log ( timestamp + "====" + diff + "   "+  new Date() + "   " + new Date(timestamp) )
        if (diff < 60)
            return "now";
        var i = 0, unit;
        while (unit = units[i++]) {
            if (diff < unit.limit || !unit.limit) {
                diff = Math.floor(diff / unit.in_seconds);
                return diff + " " + unit.name + (diff > 1 ? "s" : "") + " ago";
            }
            return;
        }
        ;
        return;
    };
    return UtilsService;
}());
exports.UtilsService = UtilsService;
//# sourceMappingURL=utils.service.js.map