import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

// import { Observable }     from 'rxjs/Observable';

import { CONFIG_API } from '../config/api.config';

@Injectable()
export class StripeService {

  private url: string;

  constructor(private http: Http) {
    this.url = CONFIG_API.URLS.stripe;
  }

  sendToken(token: any) {
    console.log('sendToken ', token);
    let headers = new Headers({ 'Content-Type': 'application/json' });

    let options = new RequestOptions({ headers: headers });
    let body = token;
    console.log(this.url)
    return this.http.post(this.url, body, options);
    // TODO catch response
    // .map(this.extractDataOb)
    // .catch(this.handleErrorOb)
  }
}
