"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_1 = require('@angular/router');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var app_routes_1 = require('./app.routes');
var app_component_1 = require('./app.component');
var data_service_1 = require('./Aservice/data.service');
var shows_component_1 = require('./shows/shows.component');
var showList_component_1 = require('./shows/showList.component');
var showDetail_component_1 = require('./shows/showDetail.component');
var venueList_component_1 = require('./venues/venueList.component');
var venueDetail_component_1 = require('./venues/venueDetail.component');
var movieList_component_1 = require('./movies/movieList.component');
var movieDetail_component_1 = require('./movies/movieDetail.component');
var Feedback_component_1 = require('./policyAndFeedback/Feedback.component');
var AboutUs_component_1 = require('./policyAndFeedback/AboutUs.component');
var Disclaimer_component_1 = require('./policyAndFeedback/Disclaimer.component');
var PrivacyPolicy_component_1 = require('./policyAndFeedback/PrivacyPolicy.component');
var social_share_cpt_1 = require('./_nm/social-share/social-share.cpt');
//import { YoutubePlayer }            from './_nm/youtube-player/youtube-player3.cpt'
var login_component_1 = require('./authAndProfile/login.component');
var profile_component_1 = require('./authAndProfile/profile.component');
var auth_service_1 = require('./Aservice/auth.service');
var window_service_1 = require('./Aservice/window.service');
var filter_service_1 = require('./Aservice/filter.service');
var stripe_service_1 = require('./Aservice/stripe.service');
var angular2_infinite_scroll_1 = require('angular2-infinite-scroll');
var spinner_cpt_1 = require('./_nm/spinner/spinner.cpt');
var googleAdsense_component_1 = require('./_nm/google-adsense/googleAdsense.component');
var core_2 = require('angular2-google-maps/core');
var ng2_bootstrap_1 = require('ng2-bootstrap/ng2-bootstrap');
var ng2_youtube_player_mini_1 = require('ng2-youtube-player-mini/ng2-youtube-player-mini');
var emailValidator_1 = require('./policyAndFeedback/emailValidator');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                router_1.RouterModule,
                http_1.HttpModule,
                app_routes_1.routing,
                forms_1.FormsModule,
                core_2.AgmCoreModule.forRoot({
                    apiKey: 'AIzaSyBlEMSEgngl6l53EQt49M2Unf1RGsPCYEQ'
                }),
                ng2_bootstrap_1.AlertModule,
                ng2_bootstrap_1.DatepickerModule,
                angular2_infinite_scroll_1.InfiniteScrollModule,
                ng2_youtube_player_mini_1.YoutubePlayerMiniModule
            ],
            declarations: [app_component_1.AppComponent,
                shows_component_1.ShowsComponent,
                showDetail_component_1.ShowDetailComponent,
                showList_component_1.ShowListComponent,
                venueList_component_1.VenueListComponent,
                venueDetail_component_1.VenueDetailComponent,
                movieList_component_1.MovieListComponent,
                movieDetail_component_1.MovieDetailComponent,
                Feedback_component_1.FeedbackComponent,
                AboutUs_component_1.AboutUsComponent,
                Disclaimer_component_1.DisclaimerComponent,
                PrivacyPolicy_component_1.PrivacyPolicyComponent,
                social_share_cpt_1.SocialShare,
                login_component_1.LoginComponent,
                profile_component_1.ProfileComponent,
                spinner_cpt_1.Spinner,
                googleAdsense_component_1.GoogleAdsenseComponent,
                emailValidator_1.EmailValidator
            ],
            providers: [auth_service_1.AuthService, data_service_1.DataService, window_service_1.WindowService, filter_service_1.FilterService, stripe_service_1.StripeService],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map