import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Response } from '@angular/http';

import { AuthService } from 'ng2-ui-auth';
// import { AuthService } from '../_nm/ng2-ui-auth/src/ng2-ui-auth.module';

@Injectable()
export class AuthenticationApi {


  constructor(private auth: AuthService) {
  }

  login(credentials:any): Observable<any> {
    return this.auth.login(credentials ).map(res => res.json());
  }

  signup(user:any): Observable<any> {
    return this.auth.signup(user).map(res => res.json());
  }

  authenticate(provider:string): Observable<Response> {
    return this.auth.authenticate(provider).map((res) => {
      console.log(res)
      return res.json()
    });
  }
}