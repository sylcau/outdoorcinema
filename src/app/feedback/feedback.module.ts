import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';
import { FormsModule }        from '@angular/forms';
import { RouterModule }       from '@angular/router';

import { SharedModule }       from '../shared/shared.module';

import { FEEDBACK_ROUTES }     from './feedback.routes';
import { FeedbackComponent }  from './feedback.component';
import { FeedbackService }    from './feedback.service';

import { AlertModule }        from 'ng2-bootstrap/components/alert';
import { SpinnerModule }      from '../_nm/spinner/spinner.module';

import { EmailValidator }                    from './emailValidator';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule,
    RouterModule.forChild(FEEDBACK_ROUTES),
    AlertModule, SpinnerModule
  ],
  declarations: [FeedbackComponent, EmailValidator],
  exports: [FeedbackComponent],
  providers: [FeedbackService]
})
export class FeedbackModule { }
