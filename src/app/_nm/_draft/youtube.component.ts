//import {Component, OnInit, Input} from 'angular2/core';

// @Component({
//   selector: 'youtube',
//   template:`

//     <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
//     <div id={{playerId}}></div>

//     <script>
//       // 2. This code loads the IFrame Player API code asynchronously.
//       var tag = document.createElement('script');

//       tag.src = "https://www.youtube.com/iframe_api";
//       var firstScriptTag = document.getElementsByTagName('script')[0];
//       firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//     </script>

//   `,

//   styles:[`

//   `],
// })
// export class YoutubeComponent implements OnInit {
//   uniqId = 1;
//   player;
//   done = false;

//   @Input() width: String;
//   @Input() height: String;
//   @Input() videoId: String;
//   @Input() playerId: String;

//   constructor(
//     private _dataService: DataService
//     ) { }

//     OnInit(){
//        console.log(this.width)
//        // player-id attr > id attr > directive-generated ID
//       //  this.playerId = attrs.playerId || element[0].id || 'unique-youtube-embed-id-' + uniqId++;
//       //       element[0].id = playerId;
//       if (!this.playerId) {
//          this.playerId = 'unique-youtube-embed-id-' + this.uniqId++;
//       }

//     }

//     onYouTubeIframeAPIReady() {
//         this.player = new YT.Player(this.playerId, {
//           height: this.height || '390',
//           width: this.width || '640',
//           videoId: this.videoId,
//           events: {
//             'onReady': this.onPlayerReady,
//             'onStateChange': this.onPlayerStateChange
//           }
//         });
//     }

//     // 4. The API will call this function when the video player is ready.
//     onPlayerReady(event) {
//       event.target.playVideo();
//     }

//     // 5. The API calls this function when the player's state changes.
//     //    The function indicates that when playing a video (state=1),
//     //    the player should play for six seconds and then stop.

//     onPlayerStateChange(event) {
//       if (event.data == YT.PlayerState.PLAYING && !this.done) {
//         setTimeout(this.stopVideo, 6000);
//         this.done = true;
//       }
//     }

//     stopVideo() {
//       this.player.stopVideo();
//     }
// }
