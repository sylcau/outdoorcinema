export interface Movie {
  __v?: number;

  _id: string;
  movieTitle: string;
  movieRef: string;

  imdbID?: string;
  img?: string;
  trailer?: string;

  parentalRating?: string;
  altTitle?: string;
  Title?: string;
  Year?: string;
  Rated?: string;
  Released?: string;
  Runtime?: string;
  Genre?: string;
  Director?: string;
  Writer?: string;
  Actors?: string;
  Plot?: string;
  Language?: string;
  Country?: string;
  Awards?: string;
  type?: string;

}
