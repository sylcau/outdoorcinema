import { NgModule }           from '@angular/core';
import { BrowserModule }      from '@angular/platform-browser';
import { APP_BASE_HREF }      from '@angular/common';
import { RouterModule }       from '@angular/router';
import { HttpModule }         from '@angular/http';
import { FormsModule }        from '@angular/forms';
import { LOCALE_ID }          from '@angular/core';

import { ROUTING_MODULE }         from './app.routes';
import { CanActivateViaAuthGuard } from './authAndProfile/auth.guard';

import { SharedModule }           from './shared/shared.module';

import { AppComponent }           from './app.component';

import { ShowsComponent }         from './shows/shows.component';
import { ShowListComponent }      from './shows/showList.component';

import { ShowDetailComponent }    from './shows/showDetail.component';

import { VenueListComponent }     from './venues/venueList.component';
import { VenueDetailComponent }   from './venues/venueDetail.component';

import { MovieDetailComponent }   from './movies/movieDetail.component';
import { MovieListComponent }     from './movies/movieList.component';

import { SocialShareModule  }            from './_nm/social-share/social-share.module';
// import { YoutubePlayer }            from './_nm/youtube-player/youtube-player3.cpt'

import { ProfileComponent }           from './authAndProfile/profile.component';
import { LoginModalComponent }        from './authAndProfile/loginModal.component';

import { InfiniteScrollModule }       from 'angular2-infinite-scroll';

import { SpinnerModule }              from './_nm/spinner/spinner.module';
// import { GoogleAdsenseComponent }  from './_nm/google-adsense/googleAdsense.component';
import { AgmCoreModule }              from 'angular2-google-maps/core';
import { AlertModule }                from 'ng2-bootstrap/components/alert';
import { DatepickerModule }           from 'ng2-bootstrap/components/datepicker';
import { ModalModule }                from 'ng2-bootstrap/components/modal';
import { AccordionModule }            from 'ng2-bootstrap/components/accordion';
import { YoutubePlayerMiniModule }    from 'ng2-youtube-player-mini';
// import { YoutubePlayerMiniModule }            from '../../../../../../git_node-modules/ng2-youtube-player-mini/dist';
// import { YoutubePlayerMiniModule }            from './_nm/ng2-youtube-player-mini';
// import { YoutubePlayerMiniModule }            from 'xxpackage';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    HttpModule,
    ROUTING_MODULE,
    FormsModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBlEMSEgngl6l53EQt49M2Unf1RGsPCYEQ'
    }),

    SharedModule.forRoot(),

    AlertModule,
    DatepickerModule,
    ModalModule,
    AccordionModule,

    InfiniteScrollModule,
    YoutubePlayerMiniModule,

    SpinnerModule,
    SocialShareModule
    // AdsenseModule
  ],
  declarations: [AppComponent,
    ShowsComponent,
    ShowDetailComponent,
    ShowListComponent,
    VenueListComponent,
    VenueDetailComponent,
    MovieDetailComponent,
    MovieListComponent,

    ProfileComponent,
    LoginModalComponent,

    // GoogleAdsenseComponent,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: '/'
    },
    { provide: LOCALE_ID, useValue: 'en-AU' },
    CanActivateViaAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
