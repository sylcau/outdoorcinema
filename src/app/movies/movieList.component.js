"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var data_service_1 = require('../Aservice/data.service');
var filter_service_1 = require('../Aservice/filter.service');
var MovieListComponent = (function () {
    function MovieListComponent(_dataService, _filterService, _router) {
        this._dataService = _dataService;
        this._filterService = _filterService;
        this._router = _router;
        this.indent = 15;
        this.movieSearch = "";
        this.maxNbMoviesDisplay = this.indent;
    }
    MovieListComponent.prototype.ngOnInit = function () {
        this.getMovies();
        // this.getDay();
    };
    MovieListComponent.prototype.getMovies = function () {
        var _this = this;
        this._dataService.getMovies().then(function (movies) {
            _this.movies = movies;
            _this.filteredMovies = _this._filterService.filter(_this.movieSearch, ['movieTitle'], _this.movies, _this.maxNbMoviesDisplay);
            console.log("nb of movies: " + movies.length);
        });
    };
    MovieListComponent.prototype.gotoDetail = function (movie) {
        var link = ['/movie', movie.movieRef];
        this._router.navigate(link);
    };
    MovieListComponent.prototype.filterChanged = function (event, maxNbMoviesDisplay) {
        if (!maxNbMoviesDisplay)
            this.maxNbMoviesDisplay = this.indent;
        console.log(this.movieSearch, maxNbMoviesDisplay);
        this.filteredMovies = this._filterService.filter(this.movieSearch, ['movieTitle'], this.movies, this.maxNbMoviesDisplay);
        //console.log(this.filteredMovies.length)
    };
    MovieListComponent.prototype.onScroll = function () {
        console.log('scrolled!!');
        this.maxNbMoviesDisplay = this.maxNbMoviesDisplay + this.indent;
        this.filterChanged(null, this.maxNbMoviesDisplay);
    };
    MovieListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'movieList',
            templateUrl: './movieList.component.html',
            styleUrls: ['./movieList.component.css']
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, filter_service_1.FilterService, router_1.Router])
    ], MovieListComponent);
    return MovieListComponent;
}());
exports.MovieListComponent = MovieListComponent;
//# sourceMappingURL=movieList.component.js.map