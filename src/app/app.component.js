"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var stripe_service_1 = require('./Aservice/stripe.service');
var router_1 = require('@angular/router');
var AppComponent = (function () {
    //private isAuthenticated: boolean;
    function AppComponent(stripeService, route, router) {
        this.stripeService = stripeService;
        this.route = route;
        this.router = router;
        this.title = 'My Outdoor Cinema';
        this.isCollapsed = true;
        this.isAtTheTop = true;
        this.isShow = false;
        this.isShow_isAtTheTop = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        //detect if page is shows to add transparent nav class
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                var url = _this.router.url;
                console.log(_this.router.url);
                if (url == "/" || url == "/shows") {
                    _this.isShow = true;
                    console.log(_this.isShow);
                }
                else {
                    _this.isShow = false;
                }
                if (_this.isShow && _this.isAtTheTop) {
                    _this.isShow_isAtTheTop = true;
                }
                else {
                    _this.isShow_isAtTheTop = false;
                }
                window.scrollTo(0, 0);
            }
        });
    };
    AppComponent.prototype.openCheckout = function () {
        var handler = window.StripeCheckout.configure({
            key: 'pk_test_oi0sKPJYLGjdvOXOM8tE8cMa',
            locale: 'auto',
            token: function (token) {
                // You can access the token ID with `token.id`.
                // Get the token ID to your server-side code for use.
                this.stripeService.sendToken(token);
            }
        });
        handler.open({
            name: 'My Outdoor Cinema',
            description: ' Great Donation',
            amount: 1000
        });
    };
    //detect scroll to modify nav background
    AppComponent.prototype.checkLocation = function (event) {
        //console.debug("Scroll Event", document.body.scrollTop);
        if (document.body.scrollTop < 70) {
            //console.debug("Scroll Event", document.body.scrollTop);            
            this.isAtTheTop = true;
        }
        else {
            this.isAtTheTop = false;
        }
        if (this.isShow && this.isAtTheTop) {
            this.isShow_isAtTheTop = true;
        }
        else {
            this.isShow_isAtTheTop = false;
        }
    };
    __decorate([
        core_1.HostListener('window:scroll', ['$event']), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', [Object]), 
        __metadata('design:returntype', void 0)
    ], AppComponent.prototype, "checkLocation", null);
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-app',
            templateUrl: 'app.component.html',
            styleUrls: ['app.component.css']
        }), 
        __metadata('design:paramtypes', [stripe_service_1.StripeService, router_1.ActivatedRoute, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map