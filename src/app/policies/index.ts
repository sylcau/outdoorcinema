/**
 * This barrel file provides the export for the lazy loaded HomeComponent.
 */
export * from './aboutus.component';
export * from './disclaimer.component';
export * from './privacypolicy.component';
export * from './policies.routes';
