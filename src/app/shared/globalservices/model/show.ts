import { Movie } from './movie';
import { Venue } from './venue';

export interface Show {
    __v?: number;
    _id: string;
    endDay: string;
    gateOpen: string;
    idRef: number;
    language: string;
    movieId: Movie;
    movieStart: string;
    startDay: string;
    subtitle: string;
    type: string;
    venueId: string | Venue;

    //
    startDay_Date?: Date;
    venue?: Venue;
};

export interface ShowSimple {
    __v?: number;
    _id: string;
    endDay: string;
    gateOpen: string;
    idRef: number;
    language: string;
    movieId: string | Movie;
    movieStart: string;
    startDay: string;
    subtitle: string;
    type: string;
    venueId: string | Venue;
};
