export interface Venue {
  __v?: number;
  _id: string;
  type: string;
  cinema: string;
  suburb: string;
  venueRef: string;
  tips: string;
  address: string;
  AddressTip: string;
  maps: string;
  urlVenue: string;
  urlAccess: string;
  description: string;
  latitude: number;
  longitude: number;

  active?: boolean;

  //extra
  fullname?: string;
}
