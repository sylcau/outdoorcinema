import { Route } from '@angular/router';
import { FeedbackComponent } from './index';

export const FEEDBACK_ROUTES: Route[] = [
  {
    path: '',
    component: FeedbackComponent
  }
];
