import { Component, OnInit, Input  } from '@angular/core';

import { Location } from '@angular/common';



@Component({
   // moduleId: module.id,
   selector: 'social-share',  // margin-left:0.3em; 
   template: `
    <div class="td-easy-social-share">
      <span >
         <a *ngFor='let l of Links' href={{l.url}}
            class="{{l.faClass}}" target="_blank"
            style="margin-right:0.3em;vertical-align:middle; color: purple">
         </a>
      </span>
     </div>
   `,
   styles: [`
   .t{ font-size:10em }
   height:50px; font-size:3em;text-align: center;`]
   // ,encapsulation: ViewEncapsulation.None
})
export class SocialShareComponent implements OnInit {

   private sites = ['twitter', 'facebook', 'linkedin', 'google-plus', 'envelope'];

   private theLink: string;
   public Links: any[] = [];

   private pageLink: string; // link to follow

   private pageTitleUri: string;

   private faClass = 'fa fa-';
   private square = '';

   private base = "http://www.myoutdoorcinema.com.au"

   @Input() shareLinks: string;
   @Input() shareTitle: string;
   @Input() shareSquare: string;
   @Input() customUrl: string;

   // = encodeURIComponent(pageTitle);

   constructor( private location: Location) { }

   ngOnInit() {

        this.pageTitleUri = encodeURIComponent(this.shareTitle);

        // check if square icon specified
        this.square = (this.shareSquare && this.shareSquare.toString() === 'true') ? '-square' : '';

        // check if customUrL
        this.pageLink = (this.customUrl) ? this.customUrl.toString() : this.base + this.location.path() ; // encodeURIComponent(location.href);
        console.log(this.pageLink);

        // assign share link for each network
        let arr = this.shareLinks.split(',');
        arr.forEach(function (key) {
          key = key.toLowerCase().trim();
          console.log(key);
          switch (key) {
            case 'twitter':
              this.theLink = 'http://twitter.com/intent/tweet?text=' +
                this.pageTitleUri + '%20' + this.pageLink;
              break;
            case 'facebook':
              this.theLink = 'http://facebook.com/sharer.php?u=' +
                this.pageLink;
              break;
            case 'linkedin':
              this.theLink = 'http://www.linkedin.com/shareArticle?mini=true&url=' +
                this.pageLink + '&title=' + this.pageTitleUri;
              break;
            case 'google-plus':
              this.theLink = 'https://plus.google.com/share?url=' + this.pageLink;
              break;
            case 'envelope':
              this.theLink = 'mailto:?subject=' + this.pageTitleUri +
                '&body=Sender suggest that you check the great shows on this link ' + this.pageLink;
              break;
            default:
              break;
          }

          if (this.sites.indexOf(key) > -1) {
            let faClass = this.faClass + key + this.square;
            this.Links.push({network: key, url: this.theLink, faClass: faClass});
          }
        }, this);

  }

}
