/**
 * This barrel file provides the exports for the shared resources (services, components).
 */
export * from './globalservices/index';
export * from './config/api.config';
