"use strict";
var Feedback = (function () {
    function Feedback(email, text) {
        this.email = email;
        this.text = text;
    }
    return Feedback;
}());
exports.Feedback = Feedback;
//# sourceMappingURL=feedback.js.map