"use strict";
var router_1 = require('@angular/router');
var shows_component_1 = require('./shows/shows.component');
var showDetail_component_1 = require('./shows/showDetail.component');
var venueList_component_1 = require('./venues/venueList.component');
var venueDetail_component_1 = require('./venues/venueDetail.component');
var movieList_component_1 = require('./movies/movieList.component');
var movieDetail_component_1 = require('./movies/movieDetail.component');
var Feedback_component_1 = require('./policyAndFeedback/Feedback.component');
var AboutUs_component_1 = require('./policyAndFeedback/AboutUs.component');
var Disclaimer_component_1 = require('./policyAndFeedback/Disclaimer.component');
var PrivacyPolicy_component_1 = require('./policyAndFeedback/PrivacyPolicy.component');
var login_component_1 = require('./authAndProfile/login.component');
var profile_component_1 = require('./authAndProfile/profile.component');
var appRoutes = [
    { path: '', redirectTo: '/shows', pathMatch: 'full' },
    { path: 'shows', component: shows_component_1.ShowsComponent },
    { path: 'show/:id', component: showDetail_component_1.ShowDetailComponent },
    { path: 'venues', component: venueList_component_1.VenueListComponent },
    { path: 'venue/:id', component: venueDetail_component_1.VenueDetailComponent },
    { path: 'movies', component: movieList_component_1.MovieListComponent },
    { path: 'movie/:id', component: movieDetail_component_1.MovieDetailComponent },
    { path: 'feedback', component: Feedback_component_1.FeedbackComponent },
    { path: 'aboutus', component: AboutUs_component_1.AboutUsComponent },
    { path: 'disclaimer', component: Disclaimer_component_1.DisclaimerComponent },
    { path: 'privacypolicy', component: PrivacyPolicy_component_1.PrivacyPolicyComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'profile', component: profile_component_1.ProfileComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
// import { provideRouter, RouterConfig } from '@angular/router';
// export const AppRoutes: RouterConfig = [
// ];
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(AppRoutes)
// ]; 
//# sourceMappingURL=app.routes.js.map