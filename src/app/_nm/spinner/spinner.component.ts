import { Component, OnInit, Input } from '@angular/core';

@Component({
  // moduleId: module.id,
  selector: 'spinner',
  template: `
   <div *ngIf="!isHidding" class="overlaySpinner" [ngStyle]="{ 'height': height }">
          <svg class="spinner spinnerBox " [ngClass]="{'in-button': inButton}" [ngStyle]="setStyles()" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
            <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
          </svg>
          <span style="font-style: italic; color: #777;">{{text}}</span>
    </div>
   `,
  styleUrls: ['./spinner.component.css']

})



export class SpinnerComponent implements OnInit {
  height: string;
  halfHeight: string;

  @Input() isHidding: any;
  @Input() boxSize: number = 65;
  @Input() text: string;
  @Input() inButton: boolean = false;


  constructor() {
    this.isHidding = false;
    //this.height = this.box + "px";
  }

  ngOnInit() {
    this.height = this.boxSize + 'px';
    this.halfHeight = (this.boxSize / 2) * -1 + 'px';
    //console.log(this.height)
    //this.height = this.box + "px";
  }

  setStyles() {
    let styles = {
      // CSS property names
      // 'font-style':  this.canSave      ? 'italic' : 'normal',  // italic
      // 'font-weight': !this.isUnchanged ? 'bold'   : 'normal',  // normal
      // 'font-size':   this.isSpecial    ? '24px'   : '8px',     // 24px
      'width': this.height ,
      'height': this.height,
      'margin-left': this.inButton ? '10px' : this.halfHeight
    };
    return styles;
  }


}
