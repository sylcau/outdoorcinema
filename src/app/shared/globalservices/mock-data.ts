//import {Hero} from './hero';
import { Show, ShowSimple } from './model/show';
import { Movie } from './model/movie';
import { Venue } from './model/Venue';

export var SHOWS: Show[] = [
	{
		'_id': '555098807bdec5d81796f51e',
		'idRef': 3,
		'movieId': {
			'_id': '554f5018b4972a3c163d3b06',
			'Genre': 'Comedy, Family',
			'Runtime': '81 min',
			'img': './img/movies/OC1003.jpg',
			'movieTitle': 'Alexander and the Terrible, Horrible, No Good, Very Bad Day',
			'movieRef': 'OC1003',
			'parentalRating': 'PG'
		},
		'venueId': '554ed6bcd617526c16816ce9',
		'startDay': '2016-04-30T16:00:00.000Z',
		'endDay': '2016-05-02T16:00:00.000Z',
		'language': '',
		'subtitle': '',
		'movieStart': '20:00',
		'gateOpen': '18:00',
		'__v': 0,
		'type': 'show'
	}, {
		'_id': '555098817bdec5d81796f520',
		'idRef': 1,
		'movieId': {
			'_id': '554f502db4972a3c163d3b19',
			'Genre': 'Drama',
			'Runtime': '102 min',
			'img': './img/movies/OC1010.jpg',
			'movieTitle': 'Calvary',
			'movieRef': 'OC1022',
			'parentalRating': 'n/a'
		},
		'venueId': '554ed6bcd617526c16816ce9',
		'startDay': '2016-01-09T16:00:00.000Z',
		'endDay': '2015-01-22T16:00:00.000Z',
		'language': '',
		'subtitle': '',
		'movieStart': '20:00',
		'gateOpen': '18:00',
		'__v': 0,
		'type': 'show'
	}, {
		'_id': '563217667b38e14c07ee4d21',
		'startDay': '2016-01-11T16:00:00.000Z',
		'endDay': '2015-10-28T16:00:00.000Z',
		'venueId': '554ed6bed617526c16816cf1',
		'gateOpen': '6:00 PM',
		'movieStart': '8:00 PM',
		'language': '',
		'subtitle': '',
		'movieId': {
			'_id': '554f502db4972a3c163d3b19',
			'movieRef': 'OC1022',
			'movieTitle': 'Buffy the Vampire Slayer',
			'Runtime': '44 min',
			'Genre': 'Action, Drama, Fantasy',
			'img': './img/movies/OC2150.jpg',
			'parentalRating': 'PG'
		},
		'idRef': 1001,
		'__v': 0,
		'type': 'show'
	}
];


export var SHOWS_Simple: ShowSimple[] = [
	{
		'_id': '555098807bdec5d81796f51e',
		'idRef': 3,
		'movieId': '554f5018b4972a3c163d3b06',
		'venueId': '554ed660287b05d4226daab5',
		'startDay': '2016-04-28T16:00:00.000Z',
		'endDay': '2015-05-35T16:00:00.000Z',
		'language': '',
		'subtitle': '',
		'movieStart': '20:00',
		'gateOpen': '18:00',
		'__v': 0,
		'type': 'show'
	}, {
		'_id': '555098817bdec5d81796f520',
		'idRef': 1,
		'movieId': '554f5018b4972a3c163d3b06',
		'venueId': '554ed660287b05d4226daab5',
		'startDay': '2015-01-08T16:00:00.000Z',
		'endDay': '2015-01-22T16:00:00.000Z',
		'language': '',
		'subtitle': '',
		'movieStart': '20:00',
		'gateOpen': '18:00',
		'__v': 0,
		'type': 'show'
	}, {
		'_id': '563217667b38e14c07ee4d21',
		'startDay': '2016-01-09T16:00:00.000Z',
		'endDay': '2015-10-28T16:00:00.000Z',
		'venueId': '554ed6bed617526c16816cf1',
		'gateOpen': '6:00 PM',
		'movieStart': '8:00 PM',
		'language': '',
		'subtitle': '',
		'movieId': '562ccfedf8d66ebc1c71920d',
		'idRef': 1001,
		'__v': 0,
		'type': 'show'
	}
];

export var MOVIES: Movie[] = [
      {
         '_id': '554f5024b4972a3c163d3b0e',
         'Genre': 'Comedy, Romance',
			'parentalRating': 'test',
         'Runtime': '97 min',
         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1011.jpg',
         'movieTitle': 'Clueless',
         'movieRef': 'OC1011'
      },
      {
         '_id': '554f5025b4972a3c163d3b0f',
         'Genre': 'Drama, Music, Romance',
						'parentalRating': 'test',

         'Runtime': '100 min',
         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1012.jpg',
         'movieTitle': 'Dirty Dancing',
         'movieRef': 'OC1012'
      },
      {
         '_id': '554f502cb4972a3c163d3b18',
         'Genre': 'Animation, Adventure, Comedy',
						'parentalRating': 'test',

         'Runtime': '102 min',
         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1021.jpg',
         'movieTitle': 'Frozen Sing-A-Long',
         'movieRef': 'OC1021'
      },
      {
         '_id': '554f502db4972a3c163d3b19',
         'Genre': 'Comedy, Fantasy, Sci-Fi',
						'parentalRating': 'test',

         'Runtime': '105 min',
         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1022.jpg',
         'movieTitle': 'Ghostbusters',
         'movieRef': 'OC1022'
      },
      {
         '_id': '554f5034b4972a3c163d3b1c',
         'Genre': 'Musical, Romance',
						'parentalRating': 'test',

         'Runtime': '110 min',
         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1025.jpg',
         'movieTitle': 'Grease Singalong',
         'movieRef': 'OC1025'
      },
      {
         '_id': '554f5057b4972a3c163d3b37',
         'Genre': 'Animation, Adventure, Family',
						'parentalRating': 'test',

         'Runtime': '77 min',
         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1052.jpg',
         'movieTitle': 'Peter Pan ',
         'movieRef': 'OC1052'
      },
      {
         '_id': '554f5078b4972a3c163d3b52',
         'Genre': 'Animation, Adventure, Family',			'parentalRating': 'test',

         'Runtime': '78 min',
         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1081.jpg',
         'movieTitle': 'The Jungle Book',
         'movieRef': 'OC1081'
      },
      {
         '_id': '554f5081b4972a3c163d3b5a',
         'Genre': 'Drama',
         'Runtime': '99 min',			'parentalRating': 'test',

         'img': 'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1089.jpg',
         'movieTitle': 'The Room',
         'movieRef': 'OC1089'
      }
];

export var GHOST: Movie = {
      '_id':'554f502db4972a3c163d3b19',
      'Actors':'Bill Murray, Dan Aykroyd, Sigourney Weaver, Harold Ramis',
      'Awards':'Nominated for 2 Oscars. Another 4 wins & 6 nominations.',
      'Country':'USA','Director':'Ivan Reitman','Genre':'Comedy, Fantasy, Sci-Fi',
      'Language':'English',
      'Plot':'Three odd-ball scientists get kicked out of their cushy positions at a university in New York City where they studied the occult. They decide to set up shop in an old firehouse and become Ghostbusters, trapping pesky ghosts, spirits, haunts, and poltergeists for money. They wise-crack their way through the city, and stumble upon a gateway to another dimension, one which will release untold evil upon the city. The Ghostbusters are called on to save the Big Apple.','Rated':'PG',
      'Released':'08 Jun 1984','Runtime':'105 min','Title':'Ghostbusters','Writer':'Dan Aykroyd, Harold Ramis',
      'Year':'1984',
      '__v':0,'altTitle':'0',
      'imdbID':'tt0121318',
      'img':'http://s3-ap-southeast-2.amazonaws.com/my-outdoor-cinema/img/movies/OC1022.jpg',
      'movieTitle':'Ghostbusters','movieRef':'OC1022','parentalRating':'PG','trailer':'vntAEVjPBzQ','type':'movie'
};

export var VENUES: Venue[] = [
      {
         '_id':'554ed6bbd617526c16816ce7',
         'venueRef':'CamMos',
         'tips':'',
         'address':'Mosarts Lochee Street, Mosman Park.',
         'AddressTip':'',
         'maps':'https://www.google.com.au/maps/place/MosArts+(Mosman+Park+Arts+Foundation)/@-32.009413,115.76339,17z/data=!3m1!4b1!4m2!3m1!1s0x2a32a4035f9d4d17:0x177efad04888497c',
         'urlVenue':'http://camelot.lunapalace.com.au/',
         'urlAccess':'http://camelot.lunapalace.com.au/info',
         'description':'A gorgeous little courtyard cinema located in one of the town halls/rep theatres of Mosman Park. Drinks and snacks can be purchased at the venue or across the street. They also have visiting food vendors such as paella or wood-fired pizzas, which vary from night to night. Showing a combination of blockbuster new movies and foreign films, seating is deck chair style with some bean bags at the front. Free Parking.',
         'latitude':-32.009264,
         'longitude':115.76338,
         '__v':0,
         'suburb':'Mosman Park',
         'cinema':'Camelot Outdoor Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bbd617526c16816ce8',
         'venueRef':'JooECU',
         'tips':'',
         'address':'270 Joondalup Drive, Joondalup 6027',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'https://perthfestival.com.au/lotterywest-festival-films',
         'urlAccess':'https://perthfestival.com.au/lotterywest-festival-films/ecu-joondalup-pines/',
         'description':'Set in the grounds of Edith Cowan University in Joondalup, this is another beautiful venue, set up for the duration of the Perth International Arts Festival. There are plenty of options for picnicking both inside and outside the venue, though please note there is No BYO. There are food and beverage options available inside. The movies shown are usually foreign films (That means subtitles, don’t forget your glasses!) as part of the PIAF. Seating is deck chair style, BYO cushion and rug.',
         'latitude':-31.752458,
         'longitude':115.770962,
         '__v':0,
         'suburb':'ECU',
         'cinema':'Joondalup Pines',
         'type':'venue'
      },
      {
         '_id':'554ed6bcd617526c16816ce9',
         'venueRef':'LunLee',
         'tips':'',
         'address':'155 Oxford Street Leederville WA 6007',
         'AddressTip':'',
         'maps':'https://www.google.com.au/maps/place/Luna+Cinemas+Leederville/@-31.936222,115.84091,17z/data=!3m1!4b1!4m2!3m1!1s0x2a32a54864c949a7:0x7730ca8af1607969',
         'urlVenue':'http://outdoor.lunapalace.com.au/home',
         'urlAccess':'http://outdoor.lunapalace.com.au/info',
         'description':'A fabulous little outdoor cinema in Leederville. Snacks and drinks are available in the main cinema venue. Mostly showing new blockbuster movies, with some special screenings, such as the cult movie “The Room”. The seating is a combination of plastic and metal outdoor chairs and deck chair style. Definitely BYO cushion. Paid Parking.',
         'latitude':-31.936221,
         'longitude':115.840913,
         '__v':0,
         'suburb':'Leederville',
         'cinema':'Luna Outdoor Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bcd617526c16816cea',
         'venueRef':'McDBas',
         'tips':'',
         'address':'BIC Reserve corner Guildford Road and Wilson Street.',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'http://www.communitycinemas.com.au/',
         'urlAccess':'http://www.communitycinemas.com.au/',
         'description':'Held in the BIC Reserve, not far from the Bassendean Train Station. As with all of the McDonald’s Community Cinemas, this venue is run by volunteers and all profits go towards children’s charities in Perth. Over the last 13 years, they have raised over $5million for charity! Seating is BYO picnic rug or low-set camp chair, otherwise a variety of seating is available for hire (bean bags, chairs, blankets). No BYO Alcohol, Snacks & drinks available to purchase. Free Parking.',
         'latitude':-31.903897,
         'longitude':115.949779,
         '__v':0,
         'suburb':'Bassendean',
         'cinema':'McDonalds Community Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bcd617526c16816ceb',
         'venueRef':'McDBur',
         'tips':'',
         'address':'Resort Drive, Burswood Park Perth WA 6000',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'http://www.communitycinemas.com.au/',
         'urlAccess':'http://www.communitycinemas.com.au/',
         'description':'Located on the banks of the river, near the grounds of the Crown Casino. As with all of the McDonald’s Community Cinemas, this venue is run by volunteers and all profits go towards children’s charities in Perth. Over the last 13 years, they have raised over $5million for charity! Seating is BYO picnic rug or low-set camp chair, otherwise a variety of seating is available for hire (bean bags, chairs, blankets). No BYO Alcohol. Snacks & drinks available to purchase. Free Parking.',
         'latitude':-31.964738,
         'longitude':115.890513,
         '__v':0,
         'suburb':'Burswood',
         'cinema':'McDonalds Community Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bdd617526c16816cec',
         'venueRef':'McDCur',
         'tips':'',
         'address':'Edinburgh Oval in Curtin University entry off Kent Street Bentley. ',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'http://www.communitycinemas.com.au/',
         'urlAccess':'http://www.communitycinemas.com.au/',
         'description':'Located in the grounds of Curtin University, a great little venue, showing new blockbuster movies. As with all of the McDonald’s Community Cinemas, this venue is run by volunteers and all profits go towards children’s charities in Perth. Over the last 13 years, they have raised over $5million for charity! Seating is BYO picnic rug or low-set camp chair, otherwise a variety of seating is available for hire (bean bags, chairs, blankets). No BYO Alcohol, Snacks & drinks available to purchase. Free Parking.',
         'latitude':-32.006604,
         'longitude':115.890197,
         '__v':0,
         'suburb':'Curtin',
         'cinema':'McDonalds Community Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bdd617526c16816ced',
         'venueRef':'McDMan',
         'tips':'',
         'address':'Quarry Adventure Park, Pebble Beach Road Meadow Springs. ',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'http://www.communitycinemas.com.au/',
         'urlAccess':'http://www.communitycinemas.com.au/',
         'description':'Held in the Quarry Adventure Park in Meadow Springs Estate, Mandurah. A great spot to come early and let the kids run off some energy! As with all of the McDonald’s Community Cinemas, this venue is run by volunteers and all profits go towards children’s charities in Perth. Over the last 13 years, they have raised over $5million for charity! Seating is BYO picnic rug or low-set camp chair. No BYO Alcohol, it is actually an alcohol free zone. Snacks & drinks available to purchase. Free Parking.',
         'latitude':-32.499181,
         'longitude':115.760523,
         '__v':0,
         'suburb':'Mandurah',
         'cinema':'McDonalds Community Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bdd617526c16816cee',
         'venueRef':'McDMur',
         'tips':'',
         'address':'Lower Bush Court in Murdoch University entry off South Street Murdoch. ',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'http://www.communitycinemas.com.au/',
         'urlAccess':'http://www.communitycinemas.com.au/',
         'description':'Located in the grounds of the Murdoch University. As with all of the McDonald’s Community Cinemas, this venue is run by volunteers and all profits go towards children’s charities in Perth. Over the last 13 years, they have raised over $5million for charity! Seating is BYO picnic rug or low-set camp chair, otherwise a variety of seating is available for hire (bean bags, chairs, blankets). No BYO Alcohol, Snacks & drinks available to purchase. Free Parking.',
         'latitude':-32.066667,
         'longitude':115.834722,
         '__v':0,
         'suburb':'Murdoch',
         'cinema':'McDonalds Community Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bed617526c16816cef',
         'venueRef':'MooKin',
         'tips':'',
         'address':'Synergy Parkland in Kings Park on May Drive',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'http://www.moonlight.com.au/',
         'urlAccess':'http://www.moonlight.com.au/perth/venue/',
         'description':'Is located in the grounds of Kings Park, near the Synergy Parkland. There is a small snack caravan and there are limited bean bags available for hire. Otherwise please bring your own picnic rug/floor cushions or low set camp chairs. BYO picnic nibbles for a movie night in the bush, showing predominantly blockbuster movies. A rug and aeroguard against the mozzies is definitely recommended. Free Parking.',
         'latitude':-31.965353,
         'longitude':115.821754,
         '__v':0,
         'suburb':'Kings Park',
         'cinema':'Moonlight Cinema',
         'type':'venue'
      },
      {
         '_id':'554ed6bed617526c16816cf0',
         'venueRef':'OpeAir',
         'tips':'',
         'address':'Riverside Drive, cnr Adelaide Terrace.',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'http://openaircinemas.com.au/perth/home',
         'urlAccess':'http://openaircinemas.com.au/perth/pages/location-5',
         'description':'Ben & Jerry’s Open Air Cinemas – Perth – A brand new venue that has only just opened in March 2015. Located in the CBD on the corner of Riverside Drive and Adelaide Terrace, just before the causeway across to Burswood. We have yet to visit this venue, but their website says; seating is bean bags and deck chairs available for hire, otherwise BYO picnic rug or low-set camp chair. No BYO Alcohol but there is a bar, with drinks and snacks available (including pizzas and ice cream!). Paid Parking. Note: they also have FREE ice cream for everyone on Sundays!',
         'latitude':-31.9624,
         'longitude':115.877238,
         '__v':0,
         'suburb':'East Perth',
         'cinema':'Openair Cinemas',
         'type':'venue'
      },
      {
         '_id':'554ed6bed617526c16816cf1',
         'venueRef':'RooNor',
         'tips':'',
         'address':'68 Roe Street, Northbridge, Western Australia',
         'AddressTip':'Top floor of the City of Perth Roe Street Carpark.',
         'maps':'',
         'urlVenue':'http://www.rooftopmovies.com.au/',
         'urlAccess':'http://www.rooftopmovies.com.au/about/',
         'description':'Located in Northbridge on top of one of the parking buildings, this is a funky retro themed cinema. They show a combination of old favourites, art-house and new movies. There is a licensed area, snacks and pizzas available (please note the pizzas need to be ordered in advance). Seating is deck chair style, BYO cushion and rug. Paid Parking.',
         'latitude':-31.948931,
         'longitude':115.8566,
         '__v':0,
         'suburb':'Northbridge',
         'cinema':'Rooftop Movies',
         'type':'venue'
      },
      {
         '_id':'554ed6bed617526c16816cf2',
         'venueRef':'SomUWA',
         'tips':'',
         'address':'35 Stirling Highway Crawley WA 6009',
         'AddressTip':'',
         'maps':'',
         'urlVenue':'https://perthfestival.com.au/lotterywest-festival-films/uwa-somerville/',
         'urlAccess':'https://perthfestival.com.au/lotterywest-festival-films/uwa-somerville/',
         'description':'A beautiful venue surrounded by pine trees on the north east edge of UWA, set up for the Perth International Arts Festival. There is a grassy space for your picnic rug and nibbles at the front of the venue, though there are also some food options available on site (the wood fired pizzas are delicious!). The movies shown are usually foreign films (That means subtitles, don’t forget your glasses!) as part of the PIAF. Seating is deck chair style, BYO cushion and rug. Free Parking.',
         'latitude':-31.977074,
         'longitude':115.820119,
         '__v':0,
         'suburb':'UWA',
         'cinema':'Somerville',
         'type':'venue'
      }
   ]
;