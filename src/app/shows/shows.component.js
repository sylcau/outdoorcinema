"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var data_service_1 = require('../Aservice/data.service');
var filter_service_1 = require('../Aservice/filter.service');
var ShowsComponent = (function () {
    function ShowsComponent(_dataService, _filterService, _router) {
        this._dataService = _dataService;
        this._filterService = _filterService;
        this._router = _router;
        this.selectedVenue = {};
        this.test = [];
        this.filteredShows = [];
        ////
        this.dt = new Date();
        this.minDate = null;
        this.formats = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY', 'shortDate'];
        this.format = this.formats[0];
        this.dateOptions = {
            formatYear: 'YY',
            startingDay: 1
        };
        this.opened = false;
        this.day = new Date();
        //this.selectedVenue = {_id: "0", cinema: "All venues", suburb: ""};
    }
    ShowsComponent.prototype.ngOnInit = function () {
        this.datepickerOpen = false;
        this.getDay();
        this.getShows();
        this.getVenues();
    };
    ShowsComponent.prototype.getVenues = function () {
        var _this = this;
        this._dataService.getVenues().then(function (vs) {
            _this.venues = vs;
            _this.venuesOption = vs.concat([]);
            _this.venuesOption.unshift({ _id: "0", cinema: "All venues", suburb: "" });
            _this.selectedVenue = _this.venuesOption[0];
        })
            .catch(function (err) { return console.log(err); });
    };
    ShowsComponent.prototype.getShows = function () {
        var _this = this;
        this._dataService.getShowsWithVenues().then(function (shows) {
            _this.shows = shows;
            _this.dayChanged(_this.day);
            console.log("slenght: " + shows.length);
        }).catch(function (err) {
            console.log("error server " + err);
            //this.shows = [];
        });
    };
    ShowsComponent.prototype.getDay = function () {
        this.day = this._dataService.getDay();
        console.log("get DAy: day is: " + this.day.toUTCString());
    };
    ShowsComponent.prototype.venueSelected = function (vs) {
        console.log(vs);
        //this.selectedVenue = v
        //find venue
        // let tmpVenue = this.venues.filter(v => {
        //   let fullname = v.cinema + " " + v.suburb
        //   if (fullname === vs) {console.log(fullname); return true}
        // })[0]
        // if (tmpVenue) this.selectedVenue = tmpVenue._id
        // else this.selectedVenue = "0"
        console.log("gg " + this.selectedVenue._id);
        if (this.shows)
            this.filteredShows = this._filterService.filterDateVenue(this.day, 'startDay', this.shows, this.selectedVenue._id, 'venueId');
    };
    ;
    ShowsComponent.prototype.clearVenue = function () {
        this.selectedVenue = this.venuesOption[0];
        this.venueSelected("clear");
        //this.selection
    };
    ShowsComponent.prototype.dayChanged = function (d) {
        this.datepickerOpen = false;
        if (typeof d.getMonth === 'function') {
            this.day = d;
            this._dataService.setDay(d);
            console.log("day changed: " + this.day.toUTCString());
        }
        //console.log()
        this.filteredShows = this._filterService.filterDateVenue(this.day, 'startDay', this.shows, this.selectedVenue._id, 'venueId');
    };
    ShowsComponent.prototype.today = function () {
        var today = new Date();
        this.dayChanged(today);
        // this._dataService.setDay(today).then(date => {
        //     this.day = date; 
        //     console.log("day is: " + this.day.toUTCString())
        // }); 
    };
    ;
    ShowsComponent.prototype.tomorrow = function () {
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        this.dayChanged(tomorrow);
        // this._dataService.setDay(tomorrow).then(date => {
        //     this.day = date; 
        //     console.log("day is: " + this.day.toUTCString())
        // });  
    };
    ;
    ShowsComponent.prototype.prevDay = function () {
        var prevD = this._dataService.addDay(this.day, -1);
        this.dayChanged(prevD);
        // this._dataService.addDay(-1).then(date => {
        //     //this.day = new Date(date.toUTCString()); 
        //     this.day = date;
        //     console.log("day is: " + this.day.toUTCString())
        // }); 
    };
    ;
    ShowsComponent.prototype.nextDay = function () {
        var nextD = this._dataService.addDay(this.day, +1);
        this.dayChanged(nextD);
        // this._dataService.addDay(+1).then(date => {
        //     //this.day = new Date(date.toUTCString()); 
        //     this.day = date;
        //     console.log("day is: " + this.day.toUTCString())
        // });
    };
    ShowsComponent.prototype.closeDatepicker = function () {
        this.datepickerOpen = false;
    };
    ;
    // onSelect(show: Show1) { 
    //     this.selectedShow = show; 
    // }
    ShowsComponent.prototype.gotoDetail = function (show) {
        var link = ['/show', show.idRef];
        this._router.navigate(link);
    };
    ShowsComponent.prototype.getDate = function () {
        return this.dt && this.dt.getTime() || new Date().getTime();
    };
    ShowsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'shows',
            templateUrl: './shows.component.html',
            styleUrls: ['./shows.component.css']
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, filter_service_1.FilterService, router_1.Router])
    ], ShowsComponent);
    return ShowsComponent;
}());
exports.ShowsComponent = ShowsComponent;
// showFilter(el){
//   let startDayDateObj = new Date(el.startDay)
//   let startDay = startDayDateObj.getFullYear() + "-" + startDayDateObj.getDate()
//   let todayStr = this.day.getFullYear() + "-" + this.day.getDate()
//   if ( startDay == todayStr ){
//       return true
//   }
//   else {
//       return false
//   }
// } 
//# sourceMappingURL=shows.component.js.map