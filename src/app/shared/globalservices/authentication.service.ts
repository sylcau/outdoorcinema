import { Injectable } from '@angular/core';
import { AuthenticationApi } from './authentication-api';
import { AuthService } from 'ng2-ui-auth';
// import { AuthService } from '../_nm/ng2-ui-auth/src/ng2-ui-auth.module';

import { AuthenticationHttpService } from './authentication-http.service';

import { Observable } from 'rxjs/Rx';
import { Response } from '@angular/http';
import { CONFIG_API } from '../config/api.config';
import { Subject } from 'rxjs/Rx';

import { User } from './model/user';


@Injectable()
export class AuthenticationService {
  // public isLoggedIn: BehaviorSubject<boolean> = BehaviorSubject.create(false)

  // private missionAnnouncedSource = new Subject<boolean>();
  // missionAnnounced$ = this.missionAnnouncedSource.asObservable();

  // Broadcast is logged In status change
  private isLoggedInSource = new Subject<boolean>();
  isLoggedIn$ = this.isLoggedInSource.asObservable();

  // broadcast if login modal is required
  private isLoginModalRequired = new Subject<boolean>();
  isLoginModalRequired$ = this.isLoginModalRequired.asObservable();

  // broadcast user details
  private userDetails = new Subject<User>();
  userDetailsB$ = this.userDetails.asObservable();

  private urls = CONFIG_API.URLS;
  private user: User;

  constructor(
    private api: AuthenticationApi,
    private auth: AuthService,
    private authHttp: AuthenticationHttpService
    ) {
  }

  setUser(user: User) {
    this.userDetails.next(user);
  }

  setIsLoginModalRequired(bool: boolean) {
    this.isLoginModalRequired.next(bool);
  }

  setIsAuth(bool: boolean) {
    this.isLoggedInSource.next(bool);
  }

  login(credentials: any): Observable<Response> {
    return Observable.create((observable: any) => {
      this.api.login(credentials).subscribe(user => {
        this.onLoginSuccess(user);
        observable.next(user);
        observable.complete();
      },
        err =>  observable.error(err.json())
      );
    });
  }

  signup(params: any) {
    return Observable.create((observable: any) => {
      this.api.signup(params).subscribe((user: any) => {
        this.auth.setToken(user.access_token);
        this.onLoginSuccess(user);
        observable.next(user);
        observable.complete();
      });
    });
  }


  authenticate(provider: string): Observable<any> {
    return Observable.create((observable: any) => {
      return this.api.authenticate(provider).subscribe((res: any) => {
        this.onLoginSuccess(res.user);
        observable.next(res.user);
        observable.complete();
      },
      err => console.log('auth error'),
      () => {
        observable.complete();
      });
    });
  }

  isAuthenticated() {
    return this.auth.isAuthenticated();
  }

  getUser(): Observable<User> {
    return Observable.create((observable: any) => {
      if ( this.isAuthenticated() ) {
        console.log('request user');
        return this.authHttp.get(this.urls.getUser).subscribe((user: User) => {
          this.user = user;
          observable.next(user);
          observable.complete();
        });
      } else {
        console.log('error: trying to get User while not auth');
        this.setIsLoginModalRequired(true);

        // TODO!!!!
        return this.userDetailsB$.subscribe((user: any) => {
          observable.next(user);
          observable.complete();
        });
      }
    });
  }

  getUserId(): Observable<string> {
    if ( this.user ) {
      return Observable.of(this.user._id);
    } else {
      // console.log("error: trying to get User while not auth")
      return this.getUser().map( (user: User) => user._id);
    }
  }

  logout() {
    this.setIsAuth(false);
    this.user = null;
    return this.auth.logout();
  }

  private onLoginSuccess(user: User) {
    console.log('this.isLoggedIn.next(true); ', user);
    this.user = user;
    this.setUser(user);
    this.setIsAuth(true);
    return user;
  }


}
