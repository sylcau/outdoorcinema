export interface Comment {
  __v?: number;

  _id?: string;
  type?: string;

  parentId: any;
  text: string;
  user: any;
  created?: number;

  //userDetails?: any;
  isEdited?: boolean;
  newText?:string;
}
