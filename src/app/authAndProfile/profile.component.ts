import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import {
  AuthenticationService, ReminderService,
  User, Reminder } from '../shared/index';

@Component({
  // moduleId: module.id,
  selector: 'profile-container',
  //template:``,
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit, OnDestroy {
  public oneAtATime: boolean = false;
  public reminders: Reminder[] = [];

  public status: Object = {
    isFirstOpen: true,
    isSecondOpen: true,
    isFirstDisabled: false // ,
    // isOpen: true
  };

  public user: User;
  private sub: any = null;
  private submittedIds: any = {};

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private reminderService: ReminderService) {
    this.reminders = [];
  }

  ngOnInit() {
    if (!this.authenticationService.isAuthenticated()) {
      this.router.navigate(['/shows']);
      return;
    }
    this.sub = this.authenticationService.getUser().subscribe((user: User) => {
      this.user = user;
      this.getReminders(user._id);
    });
  }

  removeReminder(id: string) {
    this.submittedIds[id] = '';
    this.reminderService.removeReminders(id).subscribe(
      val => {
        this.getReminders(this.user._id, id);
      },
      e => {
        console.log(e);
        delete this.submittedIds[id];
      });
  }

  getReminders(userId: string, submittedId?: string){
    this.reminderService.getReminders(userId).subscribe(
      res => {
        //this.remindersLoaded = true
        //console.log(res, res.data)
        this.reminders = res;
        if(submittedId) {
          delete this.submittedIds[submittedId];
        }
      },
      e => {
        console.log(e);
      });
  }

  submitted(id: string): boolean {
    if (typeof this.submittedIds[id] !== 'undefined') {
      return true;
    } else {
      return false;
    }

  }


  ngOnDestroy() {
    console.log('movieDetail deleted')
    if (this.sub !== null) this.sub.unsubscribe();
  }
}
