/* tslint:disable: max-line-length */
interface IAppIds {
  GOOGLE_CLIENT_ID: string;
  FACEBOOK_CLIENT_ID: string;
}

// Feel free to extend this interface
// depending on your app specific config.
export interface IConfigApi {
  ENV: string;
  apiRoot?: string;
  APIkeys?: any;
  URLS?: any;
}

let fbId = '1587716498151001'; // outdoorcinema
let apiRoot: string = '';

if  (ENV === 'development') {
  console.log("ENV: ", ENV)
  apiRoot = 'http://localhost:5000';
  fbId = '1613030518952932'; // test app for Localhost

  // apiRoot: 'http://www.myoutdoorcinema.com.au',
  // apiRoot: '',
}

export const FACEBOOK_CLIENT_ID = fbId;
export const GOOGLE_CLIENT_ID = '722512697442-e8g0nehlvt3p2eju4cutv7v05s4hg3vi.apps.googleusercontent.com';

export const CONFIG_API: IConfigApi = {
  ENV: ENV,
  apiRoot: apiRoot,
  APIkeys: {
    stripe: 'pk_live_WtyKpVSZwWeNm5eORvJrCxV0'
  },
  URLS:  {
    shows: apiRoot + '/api/v1/getShows',
    venues: apiRoot + '/api/v1/getVenues',

    movies: apiRoot + '/api/v1/getMovies',
    movieByRef: apiRoot + '/api/v1/getMovieByRef',
    moviesbySeason: apiRoot + '/api/v1/getMoviesBySeason',

    getComments: apiRoot + '/api/v1/getComments',
    getMyComments: apiRoot + '/api/v1/getMyComments',
    addComment: apiRoot + '/api/v1/addComment',
    updateComment: apiRoot + '/api/v1/updateComment',
    deleteComment: apiRoot + '/api/v1/deleteComment',

    addReminder: apiRoot + '/api/v1/reminders/addReminder',
    getReminders: apiRoot + '/api/v1/reminders/getByUserId',
    removeReminder: apiRoot + '/api/v1/reminders/removeReminder',

    postfeedback: apiRoot + '/api/v1/feedback',
    stripe: apiRoot + '/api/v1/stripe/postToken',

    users: apiRoot + '/api/v1/users',
    getUser: apiRoot + '/api/v1/users/me'
  }
};
