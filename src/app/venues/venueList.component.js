"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var data_service_1 = require('../Aservice/data.service');
var VenueListComponent = (function () {
    function VenueListComponent(_dataService, _router) {
        this._dataService = _dataService;
        this._router = _router;
    }
    VenueListComponent.prototype.ngOnInit = function () {
        this.getVenues();
        this.getPerthGeoLocation();
        // this.getDay();
    };
    ;
    VenueListComponent.prototype.getVenues = function () {
        var _this = this;
        this._dataService.getVenues()
            .then(function (venues) {
            _this.venues = venues;
            //this.markers
        })
            .catch(function (err) { return console.log("error server"); });
    };
    ;
    VenueListComponent.prototype.getPerthGeoLocation = function () {
        this.mapCenterLat = this._dataService.getMapCenter().lat;
        this.mapCenterLong = this._dataService.getMapCenter().long;
    };
    VenueListComponent.prototype.gotoDetail = function (venue) {
        var link = ['/venue', venue.venueRef];
        this._router.navigate(link);
    };
    VenueListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'venueList',
            templateUrl: './venueList.component.html',
            styleUrls: ['./venueList.component.css'],
            styles: ["\n    .sebm-google-map-container {\n      height: 300px;\n    }\n  "]
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, router_1.Router])
    ], VenueListComponent);
    return VenueListComponent;
}());
exports.VenueListComponent = VenueListComponent;
//# sourceMappingURL=venueList.component.js.map