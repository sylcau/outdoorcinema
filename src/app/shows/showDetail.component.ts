import { Component, OnInit, OnDestroy } from '@angular/core';
//import {RouteParams, ROUTER_DIRECTIVES, RouteConfig} from '@angular/router-deprecated';
import { Router, ActivatedRoute } from '@angular/router';

//import { MovieDetailComponent }   from '../movies/movieDetail.component';

import {
  DataService,
  ReminderService,
  Show, Reminder } from '../shared/globalservices/index';


@Component({
  // // moduleId: module.id,

  selector: 'show-detail',
  templateUrl: './showDetail.component.html',

  styleUrls: [`./showDetail.component.css`]

})


export class ShowDetailComponent implements OnInit, OnDestroy {
  public show: Show;
  private remindMeBool: boolean = true;
  private hasReminder: boolean = false;
  private reminder: Reminder;
  private submitted: boolean = false;
  private sub: any;

  constructor(
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private reminderService: ReminderService) {

  }

  ngOnInit() {
    this.sub = this.route.params
      .map(params => params['id'])
      .subscribe((id) => {
          console.log(typeof id, parseInt(id) );
          this.dataService.getShow(parseInt(id))
          .then(show => {
            this.show = show;
            this.checkHasReminder(show._id);
            //console.log(show)
          })
          .catch(err => console.log('error server: ', err));

      }
    );



  }

  goToVenueDetail(venueRef: string) {
    let link = ['/venue', venueRef];
    this.router.navigate(link);
  }

  checkHasReminder(showId: string) {
    this.submitted = true; // spinner
    this.reminderService.checkHasReminder(showId).subscribe(res => {
      this.submitted = false; // remove spinner
      if (res.length !== 0) {
        this.hasReminder = true;
        this.reminder = res[0];
      } else {
        this.hasReminder = false;
      }
    });
  }

  addReminder(nbOfDays: number) {
    this.submitted = true; // spinner

    let obj = { show: {_id: this.show._id} , nbDays: nbOfDays};
    this.reminderService.addReminder(obj).subscribe(res => {
          this.submitted = false; // remove spinner
          this.hasReminder = true;
          this.reminder = res.data;
          console.log('addreminder ', res);
    });
  }

  removeReminder() {
    this.submitted = true; // spinner

    let showId = this.reminder._id;
    this.reminderService.removeReminders(showId).subscribe(res => {
          this.submitted = false; // remove spinner
          this.hasReminder = false;
          this.reminder = null;
          console.log('removeReminder ', res);
    });
  }

  ngOnDestroy() {
    console.log('showDetail deleted');
    if (this.sub !== null) this.sub.unsubscribe();
  }
}
