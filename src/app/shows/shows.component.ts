import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  DataService,
  FilterService,
  Show, Venue } from '../shared/globalservices/index';

@Component({
  // moduleId: module.id,

  selector: 'shows-container',
  templateUrl: './shows.component.html',
  styleUrls: ['./shows.component.css']

})

export class ShowsComponent implements OnInit {
  shows: Show[];
  show: Show;
  venues: Venue[];
  venue: Venue;
  venuesOption: any;
  selectedVenue: any = {};
  selectionVenue: string;
  showsComingUp: Show[] = [];

  test: any = [];

  day: Date;
  datepickerOpen: boolean;
  filteredShows: Show[] = [];

  constructor(
    private _dataService: DataService,
    private _filterService: FilterService,

    private _router: Router) {
      this.day = new Date();
      // this.selectedVenue = {_id: "0", cinema: "All venues", suburb: ""};
  }

  ngOnInit() {
    this.datepickerOpen = false;
    this.getDay();
    this.getShows();
    this.getVenues();
  }

  getVenues() {
    this._dataService.getVenues().then(vs => {
      this.venues = vs;
      this.venuesOption = vs.concat([]);
      this.venuesOption.unshift({_id: '0', cinema: 'All venues', suburb: ''});
      this.selectedVenue = this.venuesOption[0];
    })
    .catch(err => console.log(err));
  }

  getShows() {
    this._dataService.getShowsWithVenues().then(shows => {
        this.shows = shows;
        this.dayChanged(this.day);
        console.log('slenght: ' + shows.length); }
    ).catch(function(err){
      console.log('error server ' + err);
      // this.shows = [];
    });
  }

   getDay() {
    this.day = this._dataService.getDay();
        console.log('get DAy: day is: ' + this.day.toUTCString());
  }

  venueSelected(vs: any) {
    console.log(vs);
    console.log('gg ' + this.selectedVenue._id);
    if (this.shows) this.filteredShows =
      this._filterService.filterDateVenue(this.day, 'startDay', this.shows, this.selectedVenue._id, 'venueId');

  };
  clearVenue() {
    this.selectedVenue = this.venuesOption[0];
    this.venueSelected('clear');
  }

  dayChanged(d: any) {
    this.datepickerOpen = false;
    if (typeof d.getMonth === 'function') {
      this.day = d;
      this._dataService.setDay(d);
      console.log('day changed: ' + this.day.toUTCString());
    }
    // console.log()
    this.filteredShows = this._filterService.filterDateVenue(this.day, 'startDay', this.shows, this.selectedVenue._id, 'venueId');
    this.showsComingUp = this._filterService.filterComingUp(this.day, 'startDay', this.shows);
  }



  today() {
    let today = new Date();
    this.dayChanged(today);
  };

  tomorrow() {
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    this.dayChanged(tomorrow);
  };

  prevDay() {
    let prevD = this._dataService.addDay(this.day, -1);
    this.dayChanged(prevD);
  };

  nextDay() {
    let nextD = this._dataService.addDay(this.day, +1);
    this.dayChanged(nextD);
  }

  closeDatepicker() {
    this.datepickerOpen = false;
  };

  gotoDetail(show: Show) {
    let link = ['/show', show.idRef];
    this._router.navigate(link);
  }


  //// For DatePicker
  public dt: Date = new Date();
  public minDate: Date = null;
  private events: Array<any>;
  // private tomorrow:Date;
  private afterTomorrow: Date;
  private formats: Array<string> = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY', 'shortDate'];
  private format = this.formats[0];
  private dateOptions: any = {
    formatYear: 'YY',
    startingDay: 1
  };
  private opened: boolean = false;

  public getDate(): number {
    return this.dt && this.dt.getTime() || new Date().getTime();
  }
}


  // showFilter(el){
  //   let startDayDateObj = new Date(el.startDay)
  //   let startDay = startDayDateObj.getFullYear() + "-" + startDayDateObj.getDate()
  //   let todayStr = this.day.getFullYear() + "-" + this.day.getDate()
  //   if ( startDay == todayStr ){
  //       return true
  //   }
  //   else {
  //       return false
  //   }
  // }
