import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// import { Ng2UiAuthModule, CustomConfig, AuthService } from './_nm/ng2-ui-auth/src/ng2-ui-auth.module';
import { Ng2UiAuthModule , CustomConfig, AuthService } from 'ng2-ui-auth';
import { GOOGLE_CLIENT_ID, FACEBOOK_CLIENT_ID, CONFIG_API } from './config/api.config';

export class MyAuthConfig extends CustomConfig {
  // http://stackoverflow.com/questions/37006008/typescript-index-signature-is-missing-in-type
  defaultHeaders = {'Content-Type': 'application/json'};
  baseUrl = CONFIG_API.URLS.users;
  providers = {
    'google': {
      // redirectUri: 'http://localhost:5555',
      clientId: GOOGLE_CLIENT_ID,
      scope: ['email', 'profile']
    },
    'facebook': {
      // callbackUrl: 'http://localhost:5000',
      clientId: FACEBOOK_CLIENT_ID
      // ,scopes: ''
    }
  };
}


import {
  DataService,
  FilterService,
  AuthenticationService,
  AuthenticationHttpService,
  AuthenticationApi,
  ReminderService,
  StripeService } from './index';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, RouterModule, Ng2UiAuthModule.getWithConfig(MyAuthConfig)],
  declarations: [],
  exports: [],
  providers: [AuthService, AuthenticationApi, AuthenticationHttpService]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [DataService, FilterService, AuthenticationService, ReminderService, StripeService]
    };
  }
}
