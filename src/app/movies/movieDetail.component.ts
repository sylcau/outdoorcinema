import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable }     from 'rxjs/Observable';

import {
  DataService,
  Show, Movie } from '../shared/index';


@Component({
  // moduleId: module.id,
  selector: 'movie-detail',
  templateUrl: './movieDetail.component.html',
  styleUrls: ['./movieDetail.component.css'],

})

export class MovieDetailComponent implements OnInit, OnDestroy {

  // @Input() ListShows: boolean = true;
  // @Input() comments: boolean = true;
  // @Input() inputMovieRef: string;

  @Input() listShows: boolean = true;
  @Input() comments: boolean = true;
  @Input() inputMovieRef: string;

  // private id: string;
  // private curSegment: RouteSegment;
  private sub: any;

  public movie: Movie;
  private shows: Show[]; //due to Date format change.
  // private venues: Venue[];
  private windowWidth: number;
  private windowHeight: number;
  private playerWidth: number;
  private playerHeight: number;
  private movieRef: string;
  private amazonLink: string;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
    ) {
      //this.movieRef = "" 
      //this.ListShows = false 
  }


   ngOnInit() {
    this.sub = this.getMovieRef().subscribe((movieRef) => {
      this.movieRef = movieRef;
      this.dataService.getMovieByRef(this.movieRef)
      .then(movie => {
        this.movie = movie[0];
        console.log('movie ', this.movie);
        this.amazonLink = 'https://www.amazon.com/gp/search?ie=UTF8&tag=104c5-20&index=instant-video&keywords=' +
            this.movie.movieTitle + ' ' + this.movie.Year;

      })
      .catch(err => console.log('error server', err));


      if (this.listShows) { // defaults false
        console.log('list Shows');
        this.dataService.getShowsByMovieRef_SortedByDates_WithVenues(this.movieRef)
        .then(shows => this.shows = shows)
        .catch(err => console.log('error server', err));

      }

    });

    this.getSize();
   }

  getMovieRef(): Observable<string> {
    console.log('Input movieRef defined?: ', this.inputMovieRef);

    if (this.inputMovieRef) {
      return Observable.of(this.inputMovieRef);
    } else {
      return this.route.params
      .map(params => params['id']);
    }
  }

  getSize() {
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;

    if (this.windowWidth > 640) {
      this.playerWidth = 640;
      this.playerHeight = 390;
    } else {
      this.playerWidth = 250;
      this.playerHeight = 152;
    }

    console.log(this.windowWidth + '  '  + this.playerWidth);

  }


//   onWindowResize(event:any) {

//     // this.windowWidth = event.target.innerWidth;
//     // this.windowHeight = event.target.innerHeight;
//     // console.log("== " + this.windowWidth + "  "  + this.windowHeight)

//     // if (this.windowWidth > 640){
//     //   this.playerWidth = 640;
//     //   this.playerHeight = 390;
//     // } else {
//     //   this.playerWidth = 250;
//     //   this.playerHeight = 152;
//     // }

//   }

  ngOnDestroy() {
    console.log('movieDetail deleted')
    this.sub.unsubscribe();
  }
}

// // $scope.$watch(
// //   function(){
// //    return $window.innerWidth;
// //   }, 
// //   function(value) {
// //    console.log(value);
// //    if (value > 640){
// //       $scope.width = 640;
// //       $scope.height = 390;
// //    } else {
// //       $scope.width = 250;
// //       $scope.height = 152;
// //    }
