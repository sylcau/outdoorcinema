import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';

import { Observable }     from 'rxjs/Observable';

import { CONFIG_API } from '../config/api.config';

// import {SHOWS, MOVIES, VENUES, GHOST} from './mock-data';
import { Show, Venue, Movie
  // ,Comment 
  } from '../index';

import { AuthenticationHttpService } from './authentication-http.service';

@Injectable()
export class DataService {
  public day: Date;
  private shows: Show[]; private showsPromise: Promise<Show[]>;
  private venues: Venue[]; private venuesPromise: Promise<Venue[]>;
  private movies: Movie[]; private moviesPromise: Promise<Movie[]>;
  // private comments: Comment[]; private commentsPromise: Promise<Comment[]>;

  private urls = CONFIG_API.URLS;



  constructor(
    private http: Http,
    private authenticationHttpService: AuthenticationHttpService
  ) {
    this.day = new Date();
    // this.day = new Date(2016, 1, 5) //Mock
    this.shows = [];
    this.venues = [];
    this.movies = [];
  }




  // *** SHOWS API ***
    getShowsOb(): Observable<Show[]> { // shows from current season
    // return observable
    console.log('nb shows ' + this.shows.length);

    if (!this.shows) {

      // dev
      let params: URLSearchParams = new URLSearchParams();
      let d = this.day;
      params.set('today', JSON.stringify( d.getTime() ) ) ;
      //

      return this.http.get(this.urls.shows, { search: params })
        .map(this.extractDataOb)
        .map(res => this.shows = res)
        .retry(3)
        .take(1)
        .catch(this.handleErrorOb);
    }

    return Observable.of(this.shows);
  };

  getShows(): Promise<Show[]> { // shows from current season
    // return Promise.resolve(SHOWS);
    console.log('nb shows ' + this.shows.length);

    if (!this.showsPromise) {

      // dev
      let params: URLSearchParams = new URLSearchParams();
      let d = this.day;
      params.set('today', d.getTime().toString());
      //

      this.showsPromise = this.http.get(this.urls.shows, { search: params })
        .toPromise()
        .then(this.extractData)
        .then(res => {
          // sort shows by Date
          res.sort((a: Show , b: Show) => {
            let date1 = new Date(a.startDay).getTime();
            let date2 = new Date(b.startDay).getTime();
            return date1 - date2;
          });

          return this.shows = res;
        })
        .catch(this.handleError);
    }

    return this.showsPromise;

  };

  getShow(idRef: number) {

    let promiseShows = this.getShows()
      .then(shows => {
        // console.log(shows);
        return shows.filter((h: Show) => h.idRef === idRef)[0];
      });
    // .then(shows => shows.filter (h => h.movieId.movieRef === movieRef))

    let result = Promise.all<any>([promiseShows, this.getVenues()])
      .then(function (res: any[]) {
        let show: Show = res[0]; // return a single object
        let venuesArr: Venue[] = res[1];

        show.venue = venuesArr.filter((v: Venue) => v._id === show.venueId)[0];
        console.log('show: ', show, show.venue);
        if (show.venue) {
          console.log(show.venue.cinema + ' ' + show.venue.suburb);
          show.venue.fullname = show.venue.cinema + ' ' + show.venue.suburb;
        } else {
          console.error('error: cannot find venue: ' + show.venueId);
        }

        show.startDay_Date = new Date(show.startDay);

        return show;
      })
      .catch(err => console.log('getShow: ', err));

    return result;
  }

  getShowsWithVenues(): Promise<Show[]> {
    let result = Promise.all<any>([this.getShows(), this.getVenues()])
      .then((res: any) => {
        res[0].forEach(function (show: Show) {
          show.venue = res[1].filter((v: Venue) => v._id === show.venueId)[0];
        });
        return res[0];
      });
    return result;
  }
  //
  getShowsByVenueRef(venueRef: string) {
    // MOCK == TODOD
    // return Promise.resolve(SHOWS) 

    return Promise.all<any>([this.getShows(), this.getVenues()])
      .then((res: any) => {
        let venueId = res[1].filter((v: Venue) => v.venueRef === venueRef)[0]._id;
        let shows = res[0].filter((s: Show) => s.venueId === venueId);
        shows.forEach((val: Show) => val.startDay_Date = new Date(val.startDay));
        console.log('getShowsByVenueRef ' + venueId + ' | nb Shows: ' + shows.length);
        return shows;
      });
  }
  // getShowAndVenues(){
  //     this.
  // };
  // getShowVenues_1movie(){

  // };

  getShowsByMovieRef_SortedByDates_WithVenues(movieRef: string) {

    let promiseShowsSorted = this.getShows()
      .then(shows => {
        return this.sortShowsByDate(shows.filter((h: Show) => h.movieId.movieRef === movieRef));
      });
    // .then(shows => shows.filter (h => h.movieId.movieRef === movieRef))

    let result = Promise.all<any>([promiseShowsSorted, this.getVenues()])
      .then(function (res: any) {
        let showArr = res[0];
        let venuesArr = res[1];

        showArr.forEach(function (val: Show) {
          /// TODO need to assign venues
          val.venue = venuesArr.filter((v: Venue) => v._id === val.venueId)[0];
          console.log(val.venue);
          if (val.venue) {
            console.log(val.venue.cinema + ' ' + val.venue.suburb);
            val.venue.fullname = val.venue.cinema + ' ' + val.venue.suburb;
          } else console.error('error: cannot find venue: ' + val.venueId);

          val.startDay_Date = new Date(val.startDay);
        });

        return showArr;
      });

    return result;

  };


  // *** MOVIES API ***     
  getMovies(): Promise<Movie[]> { // movies from current season

    if (!this.moviesPromise) {
      this.moviesPromise = this.http.get(this.urls.moviesbySeason)
        .toPromise()
        .then(this.extractData)
        .then( (movies: Movie[]) => {
          movies.sort((a: Movie , b: Movie) => {
            if (a.movieTitle < b.movieTitle) return -1;
            if (a.movieTitle > b.movieTitle) return 1;
            return 0;
          });
          return movies;
        })
        .catch(this.handleError);
    }
    return this.moviesPromise;
  };

  getMovieByRef(movieRef: string) {
    // MOCK
    // return Promise.resolve([GHOST])
    // .then(movies => movies.filter(m => m.movieRef === movieRef)[0]);
    // return Promise.resolve(GHOST)

    let params: URLSearchParams = new URLSearchParams();
    params.set('mvRef', movieRef);

    return this.http.get(this.urls.movieByRef, { search: params })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  };


  // *** VENUES API ***
  getVenues(): Promise<Venue[]> { // all venues
    // return Promise.resolve(VENUES);
    if (!this.venuesPromise) {
      this.venuesPromise = this.http.get(this.urls.venues)
        .toPromise()
        .then(this.extractData)
        .then(res => this.venues = res)
        .catch(this.handleError);
    }

    return this.venuesPromise;
  };

  getVenueByRef(venueRef: string) {
    return this.getVenues()
      .then(venues => {
        console.log('data0: ' + venueRef);
        return venues.filter(v => v.venueRef === venueRef)[0];
      });
  };

  getVenuesGeoLocation() {  // USELESS!!
    return this.getVenues()
      .then(res => {
        return res;
      });
  };

  getMapCenter() {
    return {
      lat: -31.9546529,
      long: 115.852662
    };
  }

  // *** Utils API ***    
  getDay() {
    // return Promise.resolve(this.day);

    // Mock
    return this.day;
  };

  setDay(dateObj: Date) {
    this.day = dateObj;
    return Promise.resolve(this.day);
  };

  addDay(day: Date, i: number) {
    // console.log(typeof day + " |" + day)
    day.setDate(this.day.getDate() + i);
    let d = new Date(day.toUTCString());
    this.day = d;
    return d;
  };

  sortShowsByDate(arr: Show[]) {
    return arr.sort(function (a, b) {
      let ad = new Date(a.startDay).getTime();
      let bd = new Date(b.startDay).getTime();
      // return new Date(a.startDay) - new Date(b.startDay); 
      return ad - bd;
    });
  };


  // *** COMMENTS ***
  getComments(parentId: string) {
    let params = new URLSearchParams();
    params.set('parentId', parentId);

    return this.http.get(this.urls.getComments, { search: params })
      .map(this.extractData)
      .catch(this.handleErrorOb);

  };

  getMyComments () {// (userId: string): Observable<Comment[]> {
    // let params = new URLSearchParams();
    // params.set('userId', userId);

    // return this.http.get(this.urls.getMyComments, { search: params })
    //   .map(this.extractDataOb)
    //   .catch(this.handleErrorOb);
  };
  addComment(comment: Comment) {
    // let token = this.AuthService.getToken();

    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // headers.append('Authorization', `Bearer ${token}`);
    // let options = new RequestOptions({ headers: headers });
    // return this.AuthService.getUser().flatMap(res => {

    //   comment.user = res._id;
    //   let body = JSON.stringify(comment);
    //   console.log(body);
    //   //
    //   return this.http.post(this.urls.addComment, body, options)
    //     .map(this.extractDataOb)
    //     .catch(this.handleErrorOb);
    // });


  };
  updateComment(comment: Comment) {
    // let token = this.AuthService.getToken();
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // headers.append('Authorization', `Bearer ${token}`);
    // let options = new RequestOptions({ headers: headers });

    // return this.AuthService.getUser().flatMap(res => {

    //   comment.user = res._id;
    //   let body = JSON.stringify(comment);
    //   console.log(body);
    //   //
    //   return this.http.post(this.urls.updateComment, body, options)
    //     .map(this.extractDataOb)
    //     .catch(this.handleErrorOb);
    // });
  };
  deleteComment(comment: Comment) {
    // console.log('deleteComment service');
    // let token = this.AuthService.getToken();
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // headers.append('Authorization', `Bearer ${token}`);
    // let options = new RequestOptions({ headers: headers });

    // return this.AuthService.getUser().flatMap(res => {

    //   comment.user = res._id;
    //   let body = JSON.stringify(comment);
    //   console.log(body);
    //   //
    //   return this.http.post(this.urls.deleteComment, body, options)
    //     .map(this.extractDataOb)
    //     .catch(this.handleErrorOb);
    // });

  };

  // http handle Promise:
  private extractData(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }
    let body = res.json();
    return body.data || [];
  }
  private handleError(error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = error.message || 'Server error';
    console.log(errMsg); // log to console instead
    return Promise.reject(errMsg);
  }


  private extractDataOb(res: Response) {
    let body = res.json();
    // console.log(body.data);
    return body.data || {};
  }
  private handleErrorOb(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


}


// addHero (name: string): Promise<Hero> {
//   let body = JSON.stringify({ name });
//   let headers = new Headers({ 'Content-Type': 'application/json' });
//   let options = new RequestOptions({ headers: headers });
//   return this.http.post(this.heroesUrl, body, options)
//              .toPromise()
//              .then(this.extractData)
//              .catch(this.handleError);
// }
