import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { CONFIG_API } from '../config/api.config';

import { Observable }     from 'rxjs/Observable';


// import {SHOWS, MOVIES, VENUES, GHOST} from './mock-data';
import { Reminder } from '../index';

import { AuthenticationHttpService } from './authentication-http.service';
import { AuthenticationService } from './authentication.service';


interface ReminderPost {
  reminder: Reminder;
  userId: string;
}


@Injectable()
export class ReminderService {

  private urls = CONFIG_API.URLS;
  // private user: User;

  constructor(
    private authenticationHttpService: AuthenticationHttpService,
    private auth: AuthenticationService
  ) {

    // auth.userDetails$.subscribe(
    // (user:User) => {
    //   this.user = user
    // })

  }

  // CheckReminders
  checkHasReminder(showId: string): Observable<Reminder[]> {
    if ( !this.auth.isAuthenticated() ) {
      return Observable.of([]);
    } else {
      return this.auth.getUserId().mergeMap((id: string) => {
        return this.getReminders(id)
          .map((res: Reminder[]) => res.filter(function(r){
            return r.show._id === showId;
          }) );
      });
    }
  }


  // addReminders
  addReminder(obj: Reminder): Observable<any> {
    // add reminders
    console.log('add reminder');

      return this.auth.getUserId().mergeMap((id: string) => {
        console.log('mergemapUser ', id);
        let data: ReminderPost = {
          reminder: obj,
          userId: id
        };
        return this.authenticationHttpService.post(this.urls.addReminder, data);
      });

  }

  // remove Reminders
  removeReminders(id: string) {
    return this.authenticationHttpService.put(this.urls.removeReminder , {reminderId: id} );
  }


  getReminders(userId: string): Observable<Reminder[]> {
    return this.authenticationHttpService.get(this.urls.getReminders, {userId: userId})
    .map((res: any) => res.data.filter(function(val: Reminder){
      if (typeof val.sentDate === 'undefined') return true;
      return false;
    } ) )
    .catch(this.handleErrorOb);
  }

  private extractDataOb(res: Response) {
    let body = res.json();
    console.log(body.data);
    return body.data || {};
  }
  private handleErrorOb(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


}
