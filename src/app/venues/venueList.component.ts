import { Component, OnInit } from '@angular/core';
import { Router }           from '@angular/router';

import {
  DataService,
  Venue } from '../shared/globalservices/index';

@Component({
  // moduleId: module.id,

  selector: 'venue-list',
  templateUrl: './venueList.component.html',

  styleUrls: ['./venueList.component.css' ]
})
export class VenueListComponent implements OnInit {
  public venues: Venue[];
  // private venue: Venue;

  public mapCenterLat: number;
  public mapCenterLong: number;



  constructor(
    private _dataService: DataService,
    private _router: Router) {
  }

  ngOnInit() {
    this.getVenues();
    this.getPerthGeoLocation();
    // this.getDay();
  };

  getVenues() {
    this._dataService.getVenues()
      .then(venues => {
        this.venues = venues;
        // this.markers
      })
    .catch(err => console.log('error server'));
  };

  getPerthGeoLocation() {
    this.mapCenterLat = this._dataService.getMapCenter().lat;
    this.mapCenterLong = this._dataService.getMapCenter().long;
  }

  mapClicked(e) {
    // 
  }

  gotoDetail(venue: Venue) {
    let link = ['/venue', venue.venueRef];
    this._router.navigate(link);
  }

}
