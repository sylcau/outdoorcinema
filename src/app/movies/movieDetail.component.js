"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var Observable_1 = require('rxjs/Observable');
var data_service_1 = require('../Aservice/data.service');
var MovieDetailComponent = (function () {
    function MovieDetailComponent(dataService, route) {
        this.dataService = dataService;
        this.route = route;
        this.ListShows = true;
        this.comments = true;
        //this.movieRef = "" 
        //this.ListShows = false 
    }
    MovieDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.getMovieRef().subscribe(function (movieRef) {
            _this.movieRef = movieRef;
            _this.dataService.getMovieByRef(_this.movieRef)
                .then(function (movie) {
                _this.movie = movie[0];
                console.log("movie ", _this.movie);
            })
                .catch(function (err) { return console.log("error server", err); });
            if (_this.ListShows) {
                console.log("list Shows");
                _this.dataService.getShowsByMovieRef_SortedByDates_WithVenues(_this.movieRef)
                    .then(function (shows) { return _this.shows = shows; })
                    .catch(function (err) { return console.log("error server", err); });
            }
        });
        this.getSize();
    };
    MovieDetailComponent.prototype.getMovieRef = function () {
        console.log("Input movieRef defined?: ", this.inputMovieRef);
        if (this.inputMovieRef) {
            return Observable_1.Observable.of(this.inputMovieRef);
        }
        else {
            return this.route.params
                .map(function (params) { return params['id']; });
        }
    };
    MovieDetailComponent.prototype.getSize = function () {
        this.windowWidth = window.innerWidth;
        this.windowHeight = window.innerHeight;
        if (this.windowWidth > 640) {
            this.playerWidth = 640;
            this.playerHeight = 390;
        }
        else {
            this.playerWidth = 250;
            this.playerHeight = 152;
        }
        console.log(this.windowWidth + "  " + this.playerWidth);
    };
    //   onWindowResize(event:any) {
    //     // this.windowWidth = event.target.innerWidth;
    //     // this.windowHeight = event.target.innerHeight;
    //     // console.log("== " + this.windowWidth + "  "  + this.windowHeight)
    //     // if (this.windowWidth > 640){
    //     //   this.playerWidth = 640;
    //     //   this.playerHeight = 390;
    //     // } else {
    //     //   this.playerWidth = 250;
    //     //   this.playerHeight = 152;
    //     // }
    //   }
    MovieDetailComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], MovieDetailComponent.prototype, "ListShows", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], MovieDetailComponent.prototype, "comments", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], MovieDetailComponent.prototype, "inputMovieRef", void 0);
    MovieDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'movieDetail',
            templateUrl: './movieDetail.component.html',
            //template:'',
            styleUrls: ['./movieDetail.component.css'],
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, router_1.ActivatedRoute])
    ], MovieDetailComponent);
    return MovieDetailComponent;
}());
exports.MovieDetailComponent = MovieDetailComponent;
// // $scope.$watch(
// //   function(){
// //    return $window.innerWidth;
// //   }, 
// //   function(value) {
// //    console.log(value);
// //    if (value > 640){
// //       $scope.width = 640;
// //       $scope.height = 390;
// //    } else {
// //       $scope.width = 250;
// //       $scope.height = 152;
// //    } 
//# sourceMappingURL=movieDetail.component.js.map