import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  DataService, FilterService,
  Movie } from '../shared/index';

// import { InfiniteScroll } from 'angular2-infinite-scroll';


@Component({
  // moduleId: module.id,
  selector: 'movie-list',
  templateUrl: './movieList.component.html',
  styleUrls: ['./movieList.component.css']
})
export class MovieListComponent implements OnInit {
  movies: Movie[];
  movie: Movie;
  movieSearch: string;
  filteredMovies: Movie[];
  maxNbMoviesDisplay: number;
  indent: number = 15;

  constructor(
    private _dataService: DataService,
    private _filterService: FilterService,
    private _router: Router) {
    this.movieSearch = '';
    this.maxNbMoviesDisplay = this.indent;
  }

  ngOnInit() {
    this.getMovies();

    // this.getDay();
  }

  getMovies() {
    this._dataService.getMovies().then(movies => {
      this.movies = movies;

      this.filteredMovies = this._filterService.filter(this.movieSearch, ['movieTitle'], this.movies, this.maxNbMoviesDisplay);
      console.log('nb of movies: ' + movies.length);
    }
    );
  }

  gotoDetail(movie: Movie) {
    let link = ['/movie', movie.movieRef];
    this._router.navigate(link);
  }

  filterChanged(event: any, maxNbMoviesDisplay?: number) {
    if (!maxNbMoviesDisplay) this.maxNbMoviesDisplay = this.indent;
    console.log(this.movieSearch, maxNbMoviesDisplay);
    this.filteredMovies = this._filterService.filter(this.movieSearch, ['movieTitle'], this.movies, this.maxNbMoviesDisplay);
    // console.log(this.filteredMovies.length)
  }

  onScroll() {
    console.log('scrolled!!');
    this.maxNbMoviesDisplay = this.maxNbMoviesDisplay + this.indent;
    this.filterChanged(null, this.maxNbMoviesDisplay);
  }

}
